<?
/*
Instruktionen zum Übersetzen:
Immer den Text zwischen den "" übersetzen.
Einfach den Deutschen Text durch den Text deiner Sprache ersetzen.
Wenn ein " im Text sein soll immer ein \ davon (Bespiel: Das Tool heißt \"Farmhunter\")
Die <br> bitte nicht enfernen. Sie bedeuten soviel wie neue Zeile.
Es gibt einige Sachen wie Links und Formatierungen im Text die nicht enfernt werden sollten.
Z.b <b>Text</b>  (Das Wort "Text" ist Fett)
Es gibt einige solcher Formatierungen. Immer nur das Wort übersetzen!
Links sehen so aus: <a href=einstellungen.php class=link>Einstellungen</a> hier bitte nur "Einstellungen" übersetzen.
Fett dargestellte wörter wie z.b $zufaelliges_pw (mit einem $ davor enthalten variablen Text, meist einen Namen, E-Mail o.ä
Bitte diesen text nicht verändern, höhstens dieses Wort anders positionieren wenn die Gramattik anders ist.
Die News: Da müssen nur die letzten beiden News übersetzt werden!
Bei Rückfragen einfach melden.
Kontakt E-Mail: info@protoyou.de
*/
 

//INDEX
$in_ub="Willkommen beim Farmhunter, dem Travian Farmtool";
$in_text="Mit diesem Travian Tool habt ihr die Möglichkeit eure Farmen bei Travian zu verwalten 
und über einen Link direkt den Angriff zu starten. 
Doch nicht nur das! Das Farm-Tool bezieht seine Daten direkt von Travian.
Du musst nicht alles aufwändig per Hand eingeben.
Dess Weiteren gibt es eine Farmsuche. Es werden dir potentielle Farmen,
mit ausführlichen Wachstumsdaten sowie Spieler und Allyinformationen,
vorgeschlagen die du mit einem Klick in deine Farmliste aufnehmen kannst.
Dieses Farmtool garantiert ein schnelles und einfaches Farmen.
Mit zwei Klicks hast du deine Truppen losgeschickt (Angriff -> Ok).
Der Farmhunter ist 100% Aktuell und 100% kostenlos.<br/><br/><br/>
";
$in_text2="Melde dich an und schau doch einfach mal rein"; //TODO
$in_text3="100% LEGAL und KOSTENLOS!"; //TODO

 $in_spenden_ub = "Unterstütze Farmhunter, bereits 1 € hilft!"; //TODO
 $in_spenden_text = "Der Farmhunter ist ein <u>nichtkommerzielles</u> Projekt.<br> Es wird privat programmiert und <u>kostenlos</u> zur Verfügung gestellt.
   Leider erzeugt so ein Projekt neben weiterer Zeit um alles aktuell zu halten auch <u>Serverkosten</u>.
  <br> Zur Zeit sind dies 7€ im Monat.<br><br> <b>DU</b> kannst mithelfen den Farmhunter weiter am Leben zu erhalten.
   Schon 1 Euro hilft die monatlichen Kosten zu decken.<br /><br>
   <u>Vielen Dank für deine Spende.</u>"; //TODO

 
 $index="Farmhunter - Das Travian Farmtool";
 $in_description="Mit diesem Farmhunter / Farmtool für Travian kannst du deine Farmen schnell und einfach organisieren. Das Tool bietet eine Inaktivensuche sowie eine einfache Verwaltung deiner Angriffe.";
 $schild="Jetzt Anmelden";
 
 $screenshots = "Farmhunter - Screenshots";
 $sc_ub = "Screenshots vom Farmhunter.";
 $sc_text = "Ein Tool kann noch so gut beschrieben sein,<br />
			besser ist es doch immer wenn man sicher selber<br />
			ein Bild machen kann. Hier findest du Screenshots<br />
			vom Farmhunter die dir die Features und Möglichkeiten<br />
			zeigen die du mit dem Farmhunter hast.";
			
 $sc_totop = "Nach oben";
 $sc_screen1 = "Farmenübersicht - Zentrum des Farmtools! Von hier laufen alle Angriffe";
 $sc_screen2 = "Duchsuche die Umgebung nach passenden Farmen ab<br />
 				und setze sie mit einem Klick auf deine Farmliste ";
 $sc_screen3 = "Im Angriffsverlauf siehst du ganz schnell<br />
 				was du oder dein Dual bereits abgefarmt haben ";
 $sc_screen4 = "Natürlich kann eine Farm auch ganz einfach wieder gelöscht werden.";
 $sc_screen5 = "Du hast verschiedene Einstellungen für deinen Account zu Verfügung";
 $sc_screen6 = "In der Spielersuche kannst du einen Spieler geziehlt aufrufen und hinzufügen";

 
 $in_sp1="Dieses Projekt braucht monatlich 7€ um zu überleben.";
$in_sp2= "Wenn du dieses Projekt unterstützen willst hast du <br> hier die Möglichkeit zu Spenden <font size=-2>Bisher gespendet: 0/7</font>";
$in_spent="Klicke hier um diese Meldung dauerhaft zu entfernen";
$in_we = "[Hier könnte ihre Werbung stehen]";

 //INDEX

//LOGIN
$lo_nick="Nickname";
$lo_pass="Passwort";
$lo_bleiben="Eingeloggt bleiben";
$lo_passvergessen="Passwort vergessen";
$lo_val="Einloggen";

$lo_ub="Passwort anfordern";
$lo_email="E-Mail";
$lo_verg_val="anfordern";
$lo_link="Zum Login";
$lo_lo="Login";

//LOGIN ERROR
 $lo_mss="Passwort oder Nutzername falsch";
 $lo_err_el="Bitte gib eine E-Mail an";
$lo_err_eng="Dies ist keine gültige E-Mail";
$lo_err_nn="Bitte gib einen Namen an";

//LOGIN ERROR

$lo_be="Du hast dir dein Passwort zusenden lassen";
$lo_em_ub="Farmhunter - Passwort vergessen";
$lo_em_text="Hallo $nick <br>
Auf <a href=http://www.farmhunter.com target=_blank>http://www.farmhunter.com</a> hast du dir ein neues Passwort zusenden lassen. <br><br>
Username: <b>$nick</b> <br>
Passwort: <b>$zufaelliges_pw</b><br><br>
<b>$strich</b><br>
<font size=-1>Bitte beachte, dass es sich hierbei um eine vom System automatisch \n generierte E-Mail handelt. <br>
Bitte Antworte nicht auf diese Mail.</font><br>


<br>
Mit freundlichen Grüßen,<br>
das Farmtoolteam";
$lo_em_np="Dein neues Passwort wurde dir per E-Mail geschickt";
$lo_epn="Die E-mail: $email passt nicht zum Namen: $nick";
$lo_ng="Der Nick: $nick wurde nicht gefunden";

//LOGIN

//MENÜ

$me_ss="Startseite";
$me_so="Screenshots";
$me_ko="Kontakt";
$me_re="Registrieren";
$me_ne="News";
$me_fa="Farmen";
$me_fs="Farmsuche";
$me_ta="Truppenaktivität";
$me_lo='Farm löschen';
$me_st="Standard Truppen";
$me_ei="Einstellungen";
$me_we="Welt";
$me_lou="Logout";

//MENÜ

//REGISTRIERUNG
$reg="Farmhunter - Registrierung";
$re_be="
Du kannst dich hier mit deinem Travian Account Registrieren. <br />
Der Farmhunter benötigt die Verbindung mit deinem Travian Account um dir z.b Farmvorschläge zu machen
und eine Umgebungssuche anzubieten. Außerdem wird angezeigt wer mit dir in einer Ally ist und viele weitere nützliche Sachen.<br />
<br />
<b>Wichtig:</b> <br />
Du musst hier nicht deine Travian Logindaten angeben. <br />
Du kannst dir ein beliebiges Passwort aussuchen!<br />
 Einzig der Loginname sollte der gleiche wie im Spiel sein<br /> 
 um die Zuordnung zum Travian Account herzustellen<br /><br />";//TODO
 
 $re_nn="Nickname";
 $re_email="Deine E-Mail";
 $re_ds="Deine Spielwelt";
 $re_ww="Wähle eine Welt";
 $re_tip_nick="Deine Userid findest du indem du auf dein Profil klickst<br> und die letzte 5 stellige Zahl kopierst:<br> http://www.travian.$lang/spieler.php?uid=<b>12345</b><br><br> Wenn du deine Travian Userid einträgst ist dein<br>Loginname der selbe wie in Travian";
 $re_tip_pass="Das Passwort wird verschlüsselt in unsere Datenbank eingetragen!<br>	Es wird/kann niemals im klartext zu lesen sein";
$re_pass="Passwort (min. 6 Zeichen)";
$re_pass_wied="Passwort (Wiederholung)";
$re_ouid="oder Travian Userid";
$re_ds="Deine Spielwelt";
$re_ha="Ich habe den <a href=datenschutz.php target=_blank class=link>Haftungsausschluss</a> gelesen und akzeptiere ihn";
$re_pf="(* = Pflichtfelder)";
$re_val="Anmelden";

$re_ndabei="Deine Spielwelt nicht dabei?<br><a href=\"kontakt.php?sp=1#kontakt\" class=\"link\">Spielwelt wünschen</a>";


//REG ERROR
$re_err_welt="Bitte wähle eine Welt aus";
$re_err_nick="Bitte gib einen Nicknamen oder eine Travian Userid an";
$re_err_nf="Dein angegebener Nickname oder deine Travianid konnte nicht in Welt $welt gefunden werden.<br>
Bitte gib deinen Nicknamen/Travianid an unter welchem du auch in Travian spielst <br>
Achtung: Solltest du dich neu bei Travian angemeldet haben warte bitte einen Tag.<br>
Wir bekommen die Daten der neu angemeldeten Spieler immer etwas verspätet."; // Achtung "$welt" bitte nicht ändern! Enthält Zahl zur Welt"
$re_err_na="Da du keine Welt angegeben hast, konnte der <br>Name nicht auf Verfügbarkeit überprüft werden";
$re_err_ge="Bitte gib eine gültige E-mail an!";
$re_err_nss="Die Passwörter stimmen nicht überein";
$re_err_pl="Bitte gib ein Kennwort ein";
$re_err_se="Dein Passwort muss mindestens 6 Zeichen lang sein!";
$re_err_ha="Du musst den Haftungsausschluss akzeptieren.";
$re_err_nb="Der Nickname ist bereits belegt";
$re_err_pwnv="Dieses Passwort kannst du nicht verwenden. Bitte wähle ein anderes Passwort.";
//REG ERROR

$re_erf="Willkommen $user. <br>	dein Account wurde erfolgreich angelegt.<br> Du kannst dich nun mit dem Nick: $user und deinem Passwort einloggen. <a href=index.php>Zum Login</a>"; //ACHTUNG: $user bitte nicht ändern! Enthält usernamen
$re_bese="Dir wurde eine Bestätigungsmail an $email geschickt."; //ACHTUNG: $email bitte nicht ändern! Enthält E-mail

$re_em_title="Farmtool Bestätigungsmail";
$re_em_text="Du bekommst diese Nachricht, weil du dich bei \"<a href=http://farmhunter.com>http://www.farmhunter.com</a>\" angemeldet hast.<br>
<br>
Um die vollen Funktionen deines Accounts zu nutzen, klicke bitte auf folgenden Link:<br><br>
<a target_blank href=http://farmhunter.com/email_b.php?3x7zl5=$emailcoded&userid=$userid>http://farmhunter.com/email_b.php?3x7zl5=$emailcoded&userid=$userid</a><br>
<br>
Mit freundlichen Grüßen,<br>
das Farmtoolteam";
//REGISTRIERUNG

//KONTAKT
$kontakt="Farmhunter - Kontakt / Impressum";
$ko_ub = "Kontakt / Impressum";
$ko_an="Angaben gemäß § 5 TMG<br/>
Verantwortlich ist:";
$ko_tel="Telefon";
$ko_haf="Haftungsausschluss";

$ko_kont="Kontaktaufnahme"; 
$ko_oeod="Bitte nur in Englisch oder Deutsch!";
$ko_name="Dein Name";
$ko_email="Deine E-Mail";
$ko_ru="(für Rückfragen wichtig!)";
$ko_na="Nachricht";
$ko_wa="Achtung deine IP wird aus Sicherheitsgründen mitgeschickt";
$ko_sen="Absenden per E-Mail";
$ko_zuruck="Zurück";
$ko_ok="Vielen Dank für deine Nachricht. Ich werde mich schnellstmöglich bei dir melden.";
//ERROR
$ko_err_na="Bitte gib deinen Namen an";
$ko_err_em="Bitte gib eine gültige E-Mail an.";
$ko_err_ze="Bitte gib mindestens 10 Buchstaben ein.";
//ERROR

$ko_gwunsch = "
Hallo,
Meine Spielwelt ist leider nicht im Farmhunter integriert.
Ich würde gerne auf: [HIER SPIELWELT EINTRAGEN] spielen. 
Die URL zum Server lautet: [HIER SERVERURL EINTRAGEN]

Ich trage meine Email und meinen Namen oben ein damit du mich
benachrichtigen kannst sobald die Spielwelt verfügbar ist.

Vielen Dank
Mit Freundlichem Gruß
";
//KONTAKT
 
//NEWS
$news = "Farmhunter - News";
$ne_ub="Farmhunter - Start";
$ne_wil="Willkommen";

$ne_d1="Du wechselst";
$ne_d2="in";
//xy 
$ne_d3="Sekunden zum Account";
//ACCOUNT XY
 
 $ne_error_acc1 = "Error: Dein Travian Account konnte nichtmehr im System gefunden werden.";
$ne_error_acc2 = "
		Ist dein Account eventuell gelöscht? Sollte dem so sein, <a href=einstellungen.php?d=y&lg=1#loeschen class=link>lösche bitte auch deinen Farmhunter Account!</a><br>
		Bist du dir sicher das dein Account noch existiert? <br>
		<a href=debug.php class=link>Benachrichtige unbedingt einen Admin indem du hier klickst!</a><br>
		Danke für deine Aufmerksamkeit."; 
$ne_sendreport = "Vielen Dank! Dein Report wurde erfolgreich gesendet.<br> Es wird sich bald jemand darum kümmern.";	
	
$ne_werbemoglichkeiten="Hier sind einige Banner die du in Foren/Signaturen und Webseiten platzieren kannst";
$ne_willkommen = "Willkommen auf deinem Farmhunter Dashboard.
Hier findest du aktuelle News über die Entwicklungen bei farmhunter.com.
Außerdem kannst du von hier aus direkt zur Farmliste, Farmsuche und die vielen weiteren Tools starten die dir
der Farmhunter anbietet.";

//NUR DIE LETZTEN BEIDEN NEWS ÜBERSETZTEN!
$ne_news="
Dienstag, 12.03.2013<br />
Es gibt ab sofort ein öffentliches Verzeichnis für die Sprachdateien.<br />
Wenn du eine Sprache beherrscht und Lust hast den Farmhunter zu Verbessern oder zu übersetzen<br />
darfst du diese gerne bearbeiten. <br />
Wer sich mit GIT und Bitbucket auskennt darf gerne einen Push request stellen. <br />
<a href=\"https://bitbucket.org/roest/farmhunter-lang\">Farmhunter Sprachdateien Übersicht</a><br />
Alle anderen dürfen aber gerne einfach auf \"Source\" klicken und sich den Quelltext herunterladen.<br />
Wer nicht das Wissen oder die Möglichkeit hat einen direkten Push request zu schicken kann die bearbeitete<br />
Datei ganz einfach an info@farmhunter.com senden. Eine Anleitung wie der Übersetzen funktioniert steht<br />
ganz oben in der Datei. <br />
<br />
Vielen Dank an alle Unterstützer!<br />
<br />
Mittwoch, 25.04.2012<br />
<b>Guten Tag Zusammen</b><br />
Heute hat der Farmhunter ein sehr großes Update bekommen.<br />
Es gab folgende Änderungen:<br />
<br />
<span style=\"font-weight:bold;font-size:18px;\">Alle Travian Welten werden unterstützt</a></span><br /><br />
Eventuell habt Ihr in den letzten Tagen schon gemerkt das einige Welten hinzugekommen sind.<br />
Ich habe einen komplett neuen Importer geschrieben. Außerdem wurde einiges im Hintergrund neu<br />
 entwickelt sodass ich nun sagen kann das alle mir bekannten Travian Welten unterstützt werden.<br />
Sollte eine Welt fehlen gibt es weiterhin die bekannt \"<a href=\"kontakt.php?sp=1#kontakt\">Welt wünschen</a>\" Funktion.<br />
Es ist nun sehr leicht für mich die Welten zu erweitern sodass die Unterstützung durch den Farmhunter<br />
sehr schnell nach dem Wunsch stattfinden wird.<br />
<br />
<span style=\"font-weight:bold;font-size:18px;\">T4 wird unterstützt</span><br /><br />
Travian hat in der neuen T4 Version die Datei Struktur verändert, sodass ein Angriff aus dem Farmhunter<br />
 auf eine weiße Seite geleitet wurde. Ich unterscheide nun zwischen T4 und T3.X Welt und leite automatisch<br />
 auf die richtige Seite weiter sodass nun Angriffe wieder funktionieren.<br />
Diese Anpassungen betreffen im übrigen auch die Links auf Spielerprofile, Kartenausschnitte und Allianzen.<br />
 All diese wurden für eine T4 Unterstützung angepasst.<br />
<br />
<span style=\"font-weight:bold;font-size:18px;\">Sprachdateien überarbeitet</span><br /><br />
Die Sprachdateien von Englisch und Französisch sind mit der Zeit ein wenig in die Jahre gekommen.<br />
Ich habe die Texte die in den letzten Monaten hinzugekommen sind übersetzt und in diesen Sprachen verfügbar gemacht.<br />
Sollte hier jemand sein der diese Übersetzungen überarbeiten möchte oder eine weitere Sprache unterstützen kann<br />
würde ich mich über eine kurze Nachricht freuen. Der Text und die Arbeit alles zu übersetzen hält sich in Grenzen das ist nicht so viel. <br />
<br />
<span style=\"font-weight:bold;font-size:18px;\">Neues Design</span><br /><br />
Der Farmhunter hat ein komplett neues Gewand bekommen.<br />
Ihr seht es ist alles viel hübscher aufgeräumter und auch der Internet Explorer wird jetzt voll unterstützt. <br />
Vielen Dank an dieser Stelle an Stefan von <a href=\"http://www.osdate-area.com\" target=\"_blank\">osdate-area.com</a><br />
für das erstklassige Design! <br />
<br />
<span style=\"font-weight:bold;font-size:18px;\">Facebook</span><br /><br />
Nachdem der Farmhunter nun seit über 2 Jahren aktiv ist wird es langsam mal Zeit für eine Facebook Seite.<br />
Ich verkünde hiermit das diese nun existiert:<br />
<a href=\"https://www.facebook.com/Farmhunter\">https://www.facebook.com/Farmhunter</a><br />
Ich freue mich natürlich über \"Gefällt mir\" und Kommentare sofern Ihr dort vertreten seit.<br />
<br />
<br />
Eine kleine Notiz noch an Rande, meine <a href=\"https://www.protoyou.de\" target=\"_blank\">eigene Webseite</a> ist inzwischen auch online.<br />
Ich diesem sinne viele Grüße und viel Erfolg beim Farmen. :)<br />
Tobias<br />
<br />
<hr>
Freitag, 18.02.2011<br>
Der Farmhunter wird bis zum 23.03 auf einen neuen, schnelleren und besseren Server ziehen.<br>
Es wird somit auch die zuverlässigkeit des Updates verbessert und die Performance der Webseite optimiert.<br>
Leider ist das ganze nicht ohne Risiko. Es kann daher leider sein, das der Farmhunter<br>
für einige Tage offline ist oder sonstige Fehler auftreten.<br>
Man kann mich immer unter info@farmhunter.com erreichen und ich hoffe das alles reibungslos<br>
über die Bühne gehen wird. Weitere Infos wird es hier geben sobald die Geschichte durchgeführt wurde.<br>
Gruß
<hr>
Freitag, 19.12.2010<br>
Mithilfe gesucht:<br>
Oft stoßen Leute auf den Farmhunter auf der suche wie man richtig Farmt.<br>
Ich möchte desswegen eine Sammlung: \"Tipps und Trick's - Wie Farme ich erfolgreich\", eröffnen.<br>
Kennst du die ultimative Taktik? Weißt du wann man ab besten starten sollte?<br>
Bist <b>DU</b> ein Farmer?! Dann melde dich bei mir.<br>
Du kannst jegliche Ideen, Tricks, Sammlungen und Anregungen über das <a href=kontakt.php>Kontaktformular</a> loswerden.<br>
 Natürlich ist es auch möglich deinen Namen an dem entsprechenden Tipp in der Sammlung zu hinterlassen.<br>
(Bitte schreib in deiner Nachricht eine entsprechede Notiz ob dies gewünscht ist).<br>
Ich freue mich auf deine Nachrichten.<br>
<hr>
Mittwoch, 17.November.2010<br> 
Hier sind einige Banner die du in Foren/Signaturen und Webseiten platzieren kannst.<br><br>
<div align=\"center\">
		<img src=\"/images/banner/de1.jpg\" border=\"0\"><br>
		<input class=\"reflink\" type=\"text\" size=\"52\" value=\"http://www.farmhunter.com/images/banner/de1.jpg\"><br>
	
	
		<br>
		<img src=\"/images/banner/schild.png\" border=\"0\"><br>
		<input class=\"reflink\" type=\"text\" size=\"55\" value=\"http://www.farmhunter.com/images/banner/schild.png\"><br>
		<br>
		<img src=\"/images/banner/120.jpg\" border=\"0\"><br>
		<input class=\"reflink\"  type=\"text\" size=\"52\" value=\"http://www.farmhunter.com/images/banner/120.jpg\"><br>
		<br>
		<img src=\"/images/banner/150.jpg\" border=\"0\"><br>
		<input class=\"reflink\" type=\"text\" size=\"52\" value=\"http://www.farmhunter.com/images/banner/150.jpg\"><br>
		</div>
		<br><br>
<hr>
Sonntag, 12.September.2010<br>
Es gab einige Funktionen im Notizensystem.<br>
Teiweise wurden Notizen nicht angezeigt und man konnte sie nichtmehr ändern.<br>
Ich habe diesen Fehler nun behoben.<br>
Durch die änderungen habe ich auch etwas an der Datenbank ändern müssen.<br>
Es kann daher sein das einige Notizen nichtmehr angezeigt werden oder nun beim anderen Dorf angezeigt werden.<br>
Das ändern der Notizen wenn man mehrere Zeilen schreibt funktioniert leider bisher noch nicht.<br>
Ich werde mich diesem Problem morgen widmen.<br>
edit// Problem behoben<br>
<br>
Neu sind außerdem die Rumänischen Welten.<br>
In diesem Sinne.<br>

<hr>
Sonntag, 18.July.2010<br>
Heute gab es wieder ein kleines aber feines Update.<br>
-Die Indischen Welten wurden aufgenommen.<br>
-Die Notizen Funktion wurde implementiert.<br>
<br>
Die Notizenfunktion ist zu finden auf der <a href=farmen.php>Farmliste</a><br>
In der Spalte \"Notizen\" können ab sofort maximal 500 Zeichen Notiz zur Farm geschrieben werden.<br>
Um Platz zu sparen werden nur die ersten 10 Zeichen der Notiz angezeigt, den Rest kann man lesen<br>
sobald man mit der Maus über den Text fährt.<br>
Man könnte sich z.b notieren, ob eine Farm ertragreich ist oder ob mit Truppen zu rechnen ist.<br>
Ansonsten ist das neue Feature ziemlich selbsterklärend.<br>
Viel Spaß :)<br><br>

<hr>
Sonntag, 27.Juni.2010<br>
Soeben wurde ein neue Update hochgeladen.<br>
Ich habe die Farmsuche überarbeitet.<br>
Am auffälligsten dabei ist das neue Design der Tabelle.<br>
Die Ergebnisse sind übersichtlicher, kleiner und durch Farben markiert.<br>
Das Hinzufügen der Farmen geschieht nun ohne Reload.<br>
Außerdem habe ich die Farmliste überarbeitet und <br>
habe sie nun etwas verkleinert, sowie das Farbmanagement angepasst.<br>
Aber schaut euch doch einfach ein wenig um und.<br>
Eine weitere änderung habe ich in Planung als ersatz für das Standart Truppen management.<br>
Man wird sich Notizen zu den Farmen machen können die dann in der Farmliste angezeigt werden.<br>


<br>
<hr>
   Montag, 07.Juni.2010<br>
   Hallo Zusammen,<br>
Ich musste mich in den letzten Wochen vermehrt gegen einigen Wiederstand gegen den Farmhunter verteidigen.<br>
<b>Ich musste nun um den Farmhunter weiter betreiben zu dürfen das Feature \"Standart Truppen\" deaktivieren.</b><br>
Trotzdem freue ich mich das Farmtool nun offiziell als Legal zu markieren!<br>
<br>
Ich habe schon vor einigen Tagen ein kleines Zwischenupdate hochgeladen welches folgende Features beinhaltet.<br>
<i>Link: \"Öffne Spielersuche\" funktioniert jetzt auch mit dem alten Design<br>
Erweiterte Farmensuche mit Angabe des Einwohnerwachstums<br>
Alle Dörfer einen Spielers angreifen... Link sichtbar wenn eingeklappt<br>
Löschen der Farmen ohne weiteren Seitenaufruf<br>
Behebung des Fehlers das zuviel beim Umkreis ausgewählt wird. Wenn im Feld Umkreis 20 steht, dann werden alle Dörfer aufgelistet welche 20 Felder vom eigenen Dorf entfernt sind.<br>
Design Bug (neues Design) im Internet Explorer hab ich weitgehend verbessert ist aber noch nicht komplett gefixxt<br>
Französische Welten verfügbar<br>
Welt 7 wurde aktiviert. Der Link wurde auf s7.travian.de angepasst<br>
</i>
<br>
Noch etwas zur Farmsuche:<br>
Die Ladezeiten sind leider sehr lang. Ich habe momentan keine Speed Optimierung der Datenbank vorgenommen plane aber dies so schnell wie möglich zu erledigen.<br>
Wenn ihr eine Farmsuche startet gedultet euch bitte ein wenig.<br>
Sollten die Ladezeiten zu lang sein, deaktiviert \"Erweiterte Infos über Spieler und Allianz\" und/oder wählt einen kleineren Umkreis.<br>
<br>
							<hr>
							Samstag, 27.März.2010<br>
							Ich bin ab heute für 2 Wochen (bis zum 10.04.1010) im Urlaub<br>
							Sobald ich zurück bin wird es (hoffentlich) ein größeres Update geben<br>
							Hier ein Status:<br>
							- Übersetzung ins Englische [muss überarbeitet werden]<br>
							- Kompatibel für alle Sprachen (brauche nur die Jeweiligen Sprachdateien) [Fertig]<br>
							  (Kannst du eventuell eine andere Sprache? Und hast du Lust zu übersetzen? Melde dich bei mir!)<br>
							- Verbesserte Sortierung der Farmliste sowie Farmsuche ohne neuladen [Fertig]<br>
							- Schönere Übergänge und ausklappmenüs (Farm Hinzufügen) ohne neuladen [Fertig] <br>
							- Einklappbare Farmliste. Dörfer von Spielern einklappbar [Fertig]<br>
							- Verbesserung der Farmsuche (Inaktivensucher + Mehr Filter Optionen) [Fertig]<br>
							- Anmeldedaten + Weltdaten für Internationale Welten [Fertig]<br>
							- Spielersuche [Fertig]<br> 
							<br>
						 //edit:<br>
						 <font color=red>Das Update wurde soeben durchgeführt 03.04.2010</font>
                            <br>
							<hr>
							Donnerstag, 11.März.2010<br>
							Der Farmhunter wurde übersetzt. Er ist nun in den Sprachen <br>
							Deutsch, Englisch, (bald Französisch und Russisch) verfügbar.<br>
							Es kann sein das einige Fehler auftreten. Zum Beispiel, dass<br>
							Texte nicht angezeigt werden, es keine Fehlermeldungen gibt oder Teile<br>
							Zum Beispiel eine E-Mail nicht sichtbar sind. Solltest du so etwas entdecken,<br>
							bitte eine Meldung an mich. <br>
							
							<hr>
							Montag, 01.März.2010<br>
                            Es ist geplant den Farmhunter in anderen Sprachen verfügbar zu machen.<br>
                            Du kannst flüssig Englisch oder auch eine andere Sprache?<br>
							Melde dich bei mir über das <a href=kontakt.php class=link>Kontaktformular</a> <br>
                            <hr>
							Dienstag, 23.Februar.2010<br>
                            Das neue Design ist fertig und wie man sieht aktiviert!<br>
                            Im Internet Explorer 6 und 7 kann es eventuell zu Problemen kommen.<br>
							Sollte das Design nicht richtig angezeigt werden, kann man in <br>
							den <a href=einstellungen.php class=link>Einstellungen</a> zum alten Design wechseln.<br>
                            <hr>
                            Dienstag, 16.Februar.2010<br>
                            Der Autoreload beim Klick auf \"Angriff\" in der Farmliste wurde wieder abgestellt!<br>
                            Der Spieler und Allylink für ORG und Speed wurden korrigiert.<br>
                            Es gibt einen neuen Link direkt zum Dorf auf der Karte.<br>
                            Ein direkter Link ist leider aufgrund einer Sicherheitsmaßnahme von Travian nicht möglich.<br>
                            Eventuelle Fehler bitte über das <a href=kontakt.php class=link>Kontaktformular</a> melden!<br>
                            edit// Fehler behoben, Angriffslink ist nun wieder korrekt!<br>
                            <hr>
                            Dienstag, 9.Februar.2010<br>
                            Habe heute ein wenig Feintuning gemacht!<br>
                            Es ist nun möglich bei den Standart Truppen zwischen <br>
                            Unterstützung, Normal oder Raubzug auszuwählen.<br>
                            Außerdem lädt sich die Farmtabelle ab sofort neu sobald man auf \"Angriff\" <br>
                            klickt, was zur Folge hat, dass nun auch der Angriffszeitpunkt aktualisiert wird.<br>
                            Und wie man sieht, hab ich die Newsanzeige ein bisschen übersichtlicher gemacht.<br>
                            <hr>
                            Sonntag, 7.Februar.2010<br>
                            Ab sofort kannst du 2 Farmtool Accounts miteinander Verbinden und<br>
                            so ganz einfach mehrere Travian Welten verwalten.<br>
                            Die Verbindung kannst du in deinen <a href=einstellungen.php class=link>Einstellungen</a> vornehmen.
                            <br>
                            <hr>
                            Dienstag, 4.Februar.2010<br>
                            Der Farmhunter ist wie angekündigt auf einen neue Domain umgezogen<br>
                            Er ist nun unter <a href=http://www.farmhunter.com class=link>http://www.farmhunter.com</a> erreichbar.<br>
                            <br>
                            Desweiteren wurde ein neues Feature integriert:<br>
                            Du kannst ab sofort für deine Farmen Standard Truppen definieren.<br>
                            Diese Truppen sind dann automatisch eingetragen wenn du farmst.<br>
                            Weitere Informationen dazu gibt es unter \"Standard Truppen\"<br>
                            
                            <br>
                            <br>
                                                    
                            Viel Spaß :)";


//NEWS

//FARMEN
$farmen="Farmhunter - Farmen";
$fa_um="Farmen";
$fa_afe="Felder";
$fa_ub="Farmen";
$fa_wi=" 
	Willkommen in der Farmübersicht.<br> 
						Du hast verschiedene Möglichkeiten eine Farm hinzuzufügen.<br> 
						 Du kannst eine Spielerid angeben, um alle Dörfer eines Spielers hinzuzufügen und<br> 
						 du kannst eine Dorfid angeben um ein einzelnes Dorf anzugeben.<br> ";
	
$fa_sn="Spielername";
$fa_addsh="Alle Dörfer dieses Spielers hinzufügen";
$fa_ddh="Dieses Dorf hinzufügen";
$fa_si="Spielerid";
$fa_di="Dorfid";
$fa_dn="Dorfname";	
$fa_tipp_pid='Die Spielerid findest du indem du auf das Profil des Spielers klickst<br> und die letzte 5 stellige Zahl kopierst:<br> http://www.travian.de/spieler.php?uid=<b>12345</b><br>';		
$fa_tipp_did="Die Dorfid findest du indem du auf das Dorf klickst<br> und die 6 stellige Zahl kopierst:<br> http://www.travian.de/karte.php?d=<b>123456</b>&c=9f<br>"; 
$fa_bil="Den Spieler <b>$_POST[playername]</b> hast du bereits in deiner Farmliste!";
$fa_dds="Die Dörfer des Spielers $_POST[playername] wurden erfolgreich in deine Farmliste aufgenommen";
$fa_ngf="Es konnte kein Spieler mit dem Namen $_POST[playername] gefunden werden!";
$fa_bifl="Diesen Spieler hast du bereits in deiner Farmliste!";
$fa_ddse="Die Dörfer des Spielers <b>$spielerdaten_sel[player]</b> wurden erfolgreich in deine Farmliste aufgenommen";
$fa_nsf="Es konnte kein Spieler mit der ID $_POST[playerid] gefunden werden!";
$fa_onumb="Eine ID kann nur aus Zahlen bestehen!";	
$fa_idnf="Es konnte kein Spieler mit der ID $_GET[uid] gefunden werden!";
$fa_dsif="Die Dörfer des Spielers <b>$spielerdaten_sel_list[player]</b> wurden erfolgreich in deine Farmliste aufgenommen";
$fa_ids="Das Dorf <b>$dorfdaten[village]</b> hast du bereits in deiner Farmliste!";
$fa_ea="Das Dorf <b>$_POST[dorfname]</b> des Spielers <b>$dorfdaten[player]</b> wurde erfolgreich in deine Farmliste aufgenommen";
$fa_dnnf="Es konnte kein Dorf mit dem Namen $_POST[dorfname] gefunden werden!";
$fa_sd="Dieses Dorf hast du bereits in deiner Farmliste!";
$fa_deag="Das Dorf <b>$dorfdaten_sel[village]</b> des Spielers <b>$dorfdaten_sel[player]</b> wurde erfolgreich in deine Farmliste aufgenommen";
$fa_kdmid="Es konnte kein Dorf mit der ID $_POST[dorfid] gefunden werden!";
$fa_erinfl="Das Dorf <b>$dorfdaten_del_list[village]</b> des Spielers <b>$dorfdaten_del_list[player]</b> wurde erfolgreich in deine Farmliste aufgenommen";
$fa_kdmid2="Es konnte kein Dorf mit der ID $_GET[vid] gefunden werden!";

$fa_ek="Einklappen";
$fa_hinz="Farm hinzufügen";
$fa_sor="Sortiere Farmliste nach";
$fa_sso="Standartsortierung";

$fa_tbl_d="Dorfname";
$fa_tbl_k="Koordinaten";
$fa_tbl_e="Entfernung";
$fa_tbl_ei="Einwohner";
$fa_tbl_sp="Spielername";
$fa_tbl_no="Notizen";
$fa_tbl_an="Angriffe";
$fa_tbl_la="letzter&nbsp;Angriff";
$fa_tbl_li="Link";
$fa_ang="Angriff";

$fa_heute="Heute";
$fa_gestern="Gestern";
$fa_vorgestern="Vorgestern";
$fa_uhr="Uhr";

//STRUKTUR IST: "VORGESTERN - 13:45 UHR

$fa_nonote="Keine Notiz eingetragen. <br> Klicke auf <b>edit</b> um eine neue Notiz einzutragen";
$fa_note_tite="Notiz der Farm";
//FARMEN

//VORSCHLÄGE
$vorsch="Farmhunter - Farm Vorschläge";
$vo_ub="Farmen Vorschläge";
$vo_eitext="Kurzanleitung<br>
<hr><ul>
<li>W&auml;hle ein Dorf aus.</li>
<li>W&auml;hle den Umkreis aus der um dein Dorf herrum durchsucht werden soll.</li>
<li>W&auml;hle die Anzahl der Einwohner aus, die ein Dorf h&ouml;chstens haben darf, um für dich als Farm zu gelten.</li>
<li>Dr&uuml;cke auf \"Senden\".</li></ul>";
$vo_wed="Bitte wähle ein Dorf aus";
$vo_umk="Umkreis";
$vo_maxew="max. Einwohner";
$vo_send="Senden";

$vo_d="Dorfname";
$vo_k="Koordinaten";
$vo_eigk="Eigene Koordinaten";
$vo_mf="Mögliche Farmen";
$vo_ew="Einwohner";
$vo_sn="Spielername"; 
$vo_al="Allianz";
$vo_ent="Entfernung";
$vo_hin="Hinzufügen";
$vo_au="Angriff um";
$vo_anzf="Es sind $anzahl_ausgaben mögliche Farmen im Umkreis von $um Feldern um das Dorf";
If ($ew > 0){
$vo_anzf .=" die weniger als $ew Einwohner haben";
}
If ($ew >0 AND $_POST[al] == 1){
$vo_anzf .=" und";
}
If ($_POST[al] == 1){
$vo_anzf .=" die ohne Allianz sind";
}
$vo_anzf .=".";

$vo_allos="Ohne Ally";
$vo_spsuu="Öffne Spielersuche";
$vo_spsu="Spielersuche";
$vo_spieler="Spieler";
$vo_ewinfos="Erweiterte Infos <br>über Spieler und Ally";
$vo_spieler_suche="konnte nicht gefunden werden. Es werden";
$vo_spieler_suche2="alternativen angezeigt";
$vo_fasu="Öffne Farmsuche";
$vo_nn="Name";

$vo_secureerr="Eine so große Abfrage ist aus Sicherheitsgründen leider nicht möglich<br>Bitte wähle einen kleineren Umkreis";
$vo_tip_player="
Anzahl D&ouml;rfer: $sinfo_anz_dorfer<br>
Einwohner: $gesamtew->gesamtew <br>
Ally: $vorschlaege->alliance<br>
<table border=0>
<tr><td align=center>Dorf</td><td align=center>EW</td><td align=center>Hinzuf&uuml;gen</td></tr>
<tr><td colspan=3><hr></td></tr>
$sp_doerfer
<tr><td colspan=3 align=center><a href=$link/spieler.php?uid=$vorschlaege->uid target=_blank class=link>in Travian &ouml;ffnen</a></td></tr></table>";
$vo_tip_ally=" <table border=0><tr><td align=center>Spieler</td><td align=center>Aufrufen</td></tr><tr><td colspan=2> <hr></td></tr> $sp_ally_member <tr><td colspan=3 align=center><a href=$link/allianz.php?uid=$vorschlaege->aid target=_blank class=link>Ally in Travian &ouml;ffnen</a></td></tr></table>";
$vo_tip_allysp=" <table border=0><tr><td align=center>Spieler</td><td align=center>Aufrufen</td></tr><tr><td colspan=2> <hr></td></tr> $sp_ally_membersp <tr><td colspan=3 align=center><a href=$link/allianz.php?uid=$ss_doerfer->aid target=_blank class=link>Ally in Travian &ouml;ffnen</a></td></tr></table>";

//VORSCHLÄGE

//TRUPPENAKTIVITÄT
$lastatts="Farmtool - Truppenaktivität";
$la_ub="Deine letzten 10 Angriffe";
//TRUPPENAKTIVITÄT

//FARMEN LÖSCHEN
$loeschen="Farmhunter - Farmen Löschen";
$loe_ub="Farmen Löschen";
$loe_wtext="Du willst eine Farm aus deiner Liste entfernen? Kein Problem!<br>
Hier siehst du eine Übersicht deiner eingetragenden Farmen<br>
Mit einem Klick auf das <img src=images/kreuz.gif border=0> wird die Farm <br />
(<b>nach</b> einer Sicherheitsabfrage) entfernt.";
$loe_erf="Der Eintrag wurde erfolgreich gelöscht";

$loe_dorf="Dorf <b>$farmen_select_dorf[village]</b> von <a href=$link/spieler.php?uid=$farmen_select_dorf[uid] target=_blank class=link>$farmen_select_dorf[player]</a>";
$loe_adorf="Alle Dörfer von <a href=$link/spieler.php?uid=$farmen_select_player[uid] target=_blank class=link>$farmen_select_player[player]</a>";
$loe_sec_dorf="Möchtest du wirklich das Dorf <b>$farmen_select_dorf[village]</b> des Spielers <b>$farmen_select_dorf[player]</b> aus deiner Farmliste löschen?";
$loe_sec_adorf="Möchtest du wirklich alle Dörfer des Spielers <b>$farmen_select_player[player]</b> aus deiner Farmliste löschen?";
$loe_ja="Ja";
$loe_nein="Nein";
//FARMEN LÖSCHEN

//Truppeneinstellungen

$troops="Farmhunter - Truppeneinstellungen";
$tr_ub="Truppen definieren";
$tr_text="Hier kannst du die Standart Truppen für deine Farmen bestimmen.<br>
 Diese Truppen sind dann automatisch eingetragen wenn du farmst.<br>
 Du kannst eine globale Einstellung festlegen, diese gilt dann<br>
 wenn die Farm keine einigen Einstellungen hat.<br>
 Für jede Farm kannst du einzeln die gewünschten Truppen eintragen.";
 
$tr_wef="Wähle eine Farm";
$tr_glob="Global für alle Farmen";
$tr_global="Global";
$tr_farmen="Farmen";
$tr_ok="OK";

$tr_2="Unterstützung";
$tr_3="Normal";
$tr_4="Raubzug";
$tr_do="Dorf";

$tr_eg="Dein Globaler Eintrag wurde erfolgreich gespeichert";
$tr_up="Dein Globaler Eintrag wurde erfolgreich geupdatet";
$tr_fae="Dein Eintrag für die Farm <b>$farm_daten[village]</b> wurde erfolgreich gespeichert";
$tr_fau="Dein Eintrag für die Farm <b>$farm_daten[village]</b> wurde erfolgreich geupdatet";
//Truppeneinstellungen

//EINSTELLUNGEN

$einstellungen="Farmhunter - Einstellungen";
$ei_ub="Einstellungen";
$ei_ne="Neue E-Mail";
$ei_ema="E-Mail ändern";
$ei_ng="Dies ist keine gültige E-Mail";
$ei_eegb="E-mail erfolgreich geändert. Es wurde eine Bestätigungsemail an $n_email geschickt";

$ei_apw="Altes Passwort";
$ei_npw="Neues Passwort";
$ei_npw2="Neues Passwort";
$ei_wied="(Wiederholung)";
$ei_pwan="Passwort ändern";
$ei_apwf="Das angegebene aktuelle Passwort ist fehlerhaft.";
$ei_m6="Bitte gib mindestens 6. Zeichen als neues Passwort an";
$ei_snuei="Die angegebenen Passwörter stimmen nicht überein.";
$ei_pwnv="Dieses Passwort kannst du nicht verwenden. Bitte wähle ein anderes Passwort.";
$ei_pwerf="Passwort erfolgreich geändert";

$ei_aver="Accounts verbinden";
$ei_hkmv="Hier kannst du zwei Farmtool Accounts miteinander verbinden";
$ei_nvaa="Nickname vom anderen Account";
$ei_pwvaa="Passwort vom anderen Account";
$ei_ver="Verbinden";
$ei_ponnf="Passwort oder Nutzername falsch";
$ei_dknsg="Du kannst nicht 2 gleiche Accounts miteinander verbinden";
$ei_daerf="Die Accounts $alles[name] und $var[name] wurden miteinander verbunden";

$ei_desa="Design Ändern";
$ei_ndes="Neues Design";
$ei_ades="Altes Design";

$ei_loe_pw="Dein Passwort";
$ei_loe_gr="Grund";
$ei_loe_send="Account löschen";
$ei_loe_pwl="Bitte gib dein Passwort ein";
$ei_loe_pww="Das Passwort ist falsch.";
$ei_loe_kg="Bitte gib einen Grund an damit ich den Farmhunter weiter verbessern kann.";
$ei_loe_sure="Bist du dir sicher?";
$ei_loe_yes="Ja Löschen";
$ei_loe_no="Nein nicht Löschen";
$ei_loe_erf="Dein Konto wurde erfolgreich gelöscht.";
$ei_loe_accloe="Account löschen";

//EINSTELLUNGEN

//IE
$ie="Farmhunter - Internet Explorer";
$ie_text="<h3><a href=http://www.farmhunter.com>farmhunter.com</a> ist besonders geeignet für Mozilla FireFox oder <a href=http://www.google.de/chrome target=_blank>Google Chrome</a><br> 
				Dein Browser scheint Internet Explorer zu sein.<br>
				Wir schlagen dir vor, <u>gratis</u> Mozilla FireFox herunterzuladen, indem du auf den Button klickst.</h3>
				<a href=\"#\" onclick=\"window.open('http://www.mozilla-europe.org/de/firefox/');\"><img src=\"images/firefox.gif\" border=\"0\" title=\"FireFox 3.0\" alt=\"FireFox 3.0\"></a><br>
				
				Du kannst auch mit Internet Explorer weitermachen, indem du hier klickst.<br>
				<a href=\"index.php\">Weitermachen ohne FireFox</a><br><br>
				
				Es kann sein das dein Browser unsere Internetseite nicht richtig darstellt.
				Sollte dies der Fall sein <a href=?s=d>klicke bitte hier</a>.<br>";

//IE

//DATENSCHUTZ
$datenschutz="Farmhunter - Haftungsausschluss";
$da_ub="Haftungsausschluss";

$datenschutz_text="
<p><strong>Persönliche Worte</strong></p>
<p>Diese Seite wurde erstellt um den Spielern von Travian ein arbeit erleichterndes Programm zum Farmen zu bieten.
Jegliche Daten werden mit größter Sorgfalt behandelt. Es werden keinerlei Daten an Dritte weitergegeben. 
Das Interesse dieser Seite liegt nicht darin Travian Logindaten zu er\"fischen\"! Das Passwort kann frei gewählt werden.
Einzig der Loginname sollte der gleiche wie im Spiel sein, um dem Nutzer eine optimierte an seine Bedürfnisse angepasste
Farmsuche zu bieten. Der Loginname wird zum Beispiel dafür verwendet um die Dörfer des Farmhunternutzers im Spiel Travian
anzuzeigen und eine Farmsuche um diese Dörfer anzubieten.
Um zu verhindern das Bots sowie Informationssammler Zugriff auf die vom Farmhunter gestellten Informationen bekommen ist eine 
Anmeldung nur möglich wenn der potentielle Nutzer in der entsprechenden Welt in Travian angemeldet ist.
Dieses Tool ist <b>legal</b> und darf eingesetzt werden. <!-- und wurde von den zuständigen Administratoren von Travian überprüft.-->
Sollten Probleme oder Fragen auftauchen können sie uns gerne eine E-Mail an info@farmhunter.com schicken.
In diesem Sinn viel Spaß auf http://www.farmhunter.com</p>
<p><strong>Haftung für Inhalte</strong></p> 
    <p>Die Inhalte unserer Seiten wurden mit größter Sorgfalt erstellt. 
      Für die Richtigkeit, Vollständigkeit und Aktualität der Inhalte 
      können wir jedoch keine Gewähr übernehmen. Als Diensteanbieter sind wir gemäß § 7 Abs.1 TMG für 
      eigene Inhalte auf diesen Seiten nach den allgemeinen Gesetzen verantwortlich. 
      Nach §§ 8 bis 10 TMG sind wir als Diensteanbieter jedoch nicht 
      verpflichtet, übermittelte oder gespeicherte fremde Informationen zu 
      überwachen oder nach Umständen zu forschen, die auf eine rechtswidrige 
      Tätigkeit hinweisen. Verpflichtungen zur Entfernung oder Sperrung der 
      Nutzung von Informationen nach den allgemeinen Gesetzen bleiben hiervon 
      unberührt. Eine diesbezügliche Haftung ist jedoch erst ab dem 
      Zeitpunkt der Kenntnis einer konkreten Rechtsverletzung möglich. Bei 
      Bekanntwerden von entsprechenden Rechtsverletzungen werden wir diese Inhalte 
      umgehend entfernen.</p>
    <p><strong>Haftung für Links</strong></p>
    <p>Unser Angebot enthält Links zu externen Webseiten Dritter, auf deren 
      Inhalte wir keinen Einfluss haben. Deshalb können wir für diese 
      fremden Inhalte auch keine Gewähr übernehmen. Für die Inhalte 
      der verlinkten Seiten ist stets der jeweilige Anbieter oder Betreiber der 
      Seiten verantwortlich. Die verlinkten Seiten wurden zum Zeitpunkt der Verlinkung 
      auf mögliche Rechtsverstöße überprüft. Rechtswidrige  
      Inhalte waren zum Zeitpunkt der Verlinkung nicht erkennbar. Eine permanente 
      inhaltliche Kontrolle der verlinkten Seiten ist jedoch ohne konkrete Anhaltspunkte 
      einer Rechtsverletzung nicht zumutbar. Bei Bekanntwerden von Rechtsverletzungen 
      werden wir derartige Links umgehend entfernen.</p>
    <p><strong>Urheberrecht</strong></p>
    <p>Die durch die Seitenbetreiber erstellten Inhalte und Werke auf diesen Seiten 
      unterliegen dem deutschen Urheberrecht. Die Vervielfältigung, Bearbeitung, Verbreitung und 
      jede Art der Verwertung außerhalb der Grenzen des Urheberrechtes bedürfen 
      der schriftlichen Zustimmung des jeweiligen Autors bzw. Erstellers. Downloads 
      und Kopien dieser Seite sind nicht für den privaten oder kommerziellen 
      Gebrauch gestattet. Soweit die Inhalte auf dieser Seite nicht vom Betreiber erstellt wurden, 
      werden die Urheberrechte Dritter beachtet. Insbesondere werden Inhalte Dritter als solche 
      gekennzeichnet. Sollten Sie trotzdem auf eine Urheberrechtsverletzung aufmerksam werden, bitten wir um einen entsprechenden Hinweis. 
      Bei Bekanntwerden von Rechtsverletzungen werden wir derartige Inhalte umgehend entfernen.</p>
     </p>
<p>
Diese Seite ist ein privates und kostenloses Projekt zum Browserspiel Travian.
Diese Seite gehört nicht zur Travian Games GmbH, weder in Teilen noch im Ganzen. Diese Seite ist ebenfalls nicht im Auftrag der Travian Games GmbH entstanden.
Die Marke \"Travian\" ist ein eingetragenes Markenzeichen und gehört der Travian Games GmbH. Alle Rechte dieser Marke liegen bei der Travian Games GmbH.

Verwendung der Grafiken mit freundlicher genemigung von Travian.
";
//DATENSCHUTZ

//EMAIL BESTÄTIGUNG

$eb_title="Farmtool - E-Mail Bestätigung";
$eb_ub="E-Mail-Adressen Bestätigung";
$eb_erf="Deine E-Mail-Adresse wurde erfolgreich bestätigt!";
//EMAIL BESTÄTIGUNG

//SONSTIGES

$ubersetzt_from="Übersetzt von Deutsch ins [hier sprache einsetzen] von [hier Namen einfügen]";
$anybugs="Solltest du Fehler in der Übersetzung, Fehlende Texte oder Texte in einer anderen Sprache feststellen melde dies Bitte möglichst genau über das <a href=kontakt.php>Kontaktformular</a>";

//GERNE DARFST DU AUCH EINEN BACKLINK AUF DEINEN NAMEN SETZTEN

//SONSTIGES
?>
