<?
/*
Instruktionen for translation.
Simple translate the text between ""
Replace the English Text with your language.
If you need an " in the Text please use \ bevore. (Example: The Name of the Tool is \"Farmhunter\")
Please do not remove the <br> tags.
There are some formattings and hyperlinks in the text please let them in your text.
Example: <b>Text</b> (The word "Text" is bold)
An Hyperlink looks like:
<a href=einstellungen.php class=link>Settings</a> 
Please translate one "Settings" here.
You can use some Variables in the Text. ($zufaelliges_pw)
Just let this variable as it is - just move it maybe to another position if you need it.
If you have questions: Contact me: info@protoyou.de
*/


//INDEX
$in_ub="Welcome to Farmhunter, the Travian Farming Tool";
$in_text=" this Tool gives you the possibility to organize Travian
<br>
 and to directly start a farming attack via link. <br>
 But not just that! The Farm Tool gets it's Data’s straight from Travian.<br>
 You don’t need to type everything in by hand.<br>
 There is also a Farm search. It suggests potential Farms <br>
 with detailed information about players and alliances<br>
 Which you can add to your farming list with one click.<br>
 You can define Troops which get automatically registered for farming.<br>
 This Farm Tool guarantees you easy and fast farming<br>
 <br>
";

$in_text2="Just register and check it out"; //TODO
$in_text3="100% LEGAL and FREE!"; //TODO

$in_spenden_ub = "You can support the Farmhunter, 1 € help!"; //TODO
 $in_spenden_text = "The Farmhunter is a <u>non commercial</u> project.<br> It's programmed private and is provided <u>free of charge</u>.
   Unfortunately, this projekts need a server. So it need time to keep everything current and money to be <b>aviable for you</b>.
  <br>Currently these are 7€ per Month for the server
	<br><br> <b>YOU</b> can help to keep the Farmhunter alive.
   Already 1 Euro helps to cover the monthly cost.<br /><br>
   <u>Thank you for your donation.</u>"; //TODO

 $index="Farmhunter - The Travian Farm Tool";
 $in_description="With this Farmhunter / Farm Tool for Travian you can organize your farms fast and easy.";
 $schild="Register";
 
 $screenshots = "Farmhunter - Screenshots"; 
  $sc_ub = "Screenshots of Farmhunter.";
 $sc_text = "A tool can be described so well,<br />
			it's better if you can see some pictures.<br />
			Here are some Screenshots from the internal sites of<br />
			the farmhunter. You see the features and the <br />
			possibilities you had with the farmhunter.";
			
 $sc_totop = "to top";
 $sc_screen1 = "Farmoverview - Centrum of the Farmtool! You can start all your atts from here";
 $sc_screen2 = "Search the area for good farms<br />
 				and set them on you farmlist with only one click";
 $sc_screen3 = "In the att history you see what<br />
 				you or your second player already had farmed";
 $sc_screen4 = "Of course you can delete an farm whenever you want";
 $sc_screen5 = "You have a couple of settings for you Farmhunter Account";
 $sc_screen6 = "If you search for a specific player to farm him just use the playersearch.";
 			
  $sc_description = "Look for screenshots from the Farmhunter";
$ko_description = "Contact the Farmhunter Administrator";
$re_description = "Register for the Farmhunter";
$da_description = "We protect your data with full Security";

  $in_sp1="This Project needs 7€ per month to pay for the server.";
$in_sp2= "If you wish to support this project <br> you can donate here. <font size=-2>Donated so far: 0/7€</font>";
$in_spent="click here to not show this message anymore";
$in_we = "[your ad here]";
//INDEX

//LOGIN
$lo_nick="Nickname";
$lo_pass="Password";
$lo_bleiben="Stay logged in";
$lo_passvergessen="Forgot Password";
$lo_val="Login";

$lo_ub="Request Password";
$lo_email="E-Mail";
$lo_verg_val="request";
$lo_link="To Login";
$lo_lo="Login";

//LOGIN ERROR
 $lo_mss="Password or Username wrong";
 $lo_err_el="Please enter your email";
$lo_err_eng="This is not a valid email";
$lo_err_nn="Please enter your name";

//LOGIN ERROR

$lo_be="Your password for Farmhunter.com";
$lo_em_ub="Farmhunter - Forgot Password ";
$lo_em_text="Hello $nick <br>
On <a href=http://www.farmhunter.com target=_blank>http://www.farmhunter.com</a> you requested a new password. <br><br>
Username: <b>$nick</b> <br>
Passwort: <b>$zufaelliges_pw</b><br><br>
<b>$strich</b><br>
<font size=-1>Please note that this is an automatically generated email. <br>
Please don’t answer to this email.</font><br>


<br>
Sincerely,<br>
the Farm Tool Team";
$lo_em_np="Your password was sent to you via email";
$lo_epn="The E-mail: $email doesn’t match the name: $nick";
$lo_ng="The Nick: $nick not found";

//LOGIN

//MENÜ

$me_ss="Main Page";
$me_so="Screenshots";
$me_ko="Contact";
$me_re="Register";
$me_ne="News";
$me_fa="Farms";
$me_fs="Farm search";
$me_ta="Troop activity";
$me_lo='Erase farm';
$me_st="Standard Troops";
$me_ei="Preferences";
$me_we="World";
$me_lou="Logout";

//MENÜ

//REGISTRIERUNG
$reg="Farmhunter - Registration";
$re_be="<b><h3>Important</h3></b> 
You don’t have to type in your Travin login data. <br>
You can choose a password!<br>
 Only your ingame name is required to be the same<br> 
 To give you farm suggestions <br><br>";
 
 $re_nn="Nickname";
 $re_email="Your email";
 $re_ds="Your world";
 $re_ww="choose a world";
 $re_tip_nick="You find your user ID by clicking on your profile<br> and copying the number at the end of the url:<br> http://www.travian.$lang/spieler.php?uid=<b>12345</b><br><br> if you type in your user ID then your<br>Login name is the same as in Travian";
 $re_tip_pass="your password is encrypted in our data base!<br>	there is no way to read it clearly";
$re_pass="Password (min. 6 letters or signs)";
$re_pass_wied="Password (Repeat)";
$re_ouid="or Travian User ID";
$re_ds="Your World";
$re_ha="I read the  <a href=datenschutz.php target=_blank class=link>disclaimer</a> and i accept it.";
$re_pf="(* = Mandatory)";
$re_val="Register";

$re_ndabei="Your game world is missing?<br><a href=\"kontakt.php?sp=1#kontakt\" class=\"link\">Wish your gameworld</a>";


//REG ERROR
$re_err_welt="please choose a World";
$re_err_nick="Please enter a nickname or a User ID";
$re_err_nf="your specified nickname or Travian ID could not be found on  $welt.<br>
Please enter your nickname or Travian ID with which you are registered in Travian "; // Achtung please don’t change "$welt" ! Includes number to world"
$re_err_na="since you have specified no world your <br>name couldn’t be checked for availability";
$re_err_ge="please enter a valid email!";
$re_err_nss="the passwords do not match";
$re_err_pl="please enter a password";
$re_err_se=" Your password must be at least 6 characters long!";
$re_err_ha="you have to accept the disclaimer.";
$re_err_nb="this nickname is already in use ";
$re_err_pwnv="you can't pick this password. Please choose a different one.";
//REG ERROR

$re_erf="Welcome $user. <br>	your Account was successfully created."; //ACHTUNG: $user don’t change! Includes username
$re_bese="a confirmation email was sent to $email."; //ACHTUNG: $email don’t change! Includes E-mail

$re_em_title="Farm tool confirmation email";
$re_em_text="you get this message, because you registered at \"<a href=http://farmhunter.com>http://www.farmhunter.com</a>\".<br>
<br>
To use all the functions of your account please activate it with this Link:<br><br>
<a target_blank href=http://farmhunter.com/email_b.php?3x7zl5=$emailcoded&userid=$userid>http://farmhunter.com/email_b.php?3x7zl5=$emailcoded&userid=$userid</a><br>
<br>
Sincerely <br>
the Farmhunter team";
//REGISTRIERUNG

//KONTAKT
$kontakt="Farmhunter - contact / Imprint";
$ko_an="Information provided by § 5 TMG<br/>
Responsible:";
$ko_tel="Phone";
$ko_haf="Disclaimer";

$ko_kont="Contact";
$ko_oeod="please <b>only</b> in English or German.";
$ko_name="your name";
$ko_email="your email";
$ko_ru="(important for further inquiries!)";
$ko_na="message";
$ko_wa="Attention Your IP will be sent along for safety reasons ";
$ko_sen="send by email";
$ko_zuruck="Back";
$ko_ok="Thank you for your message. I will answer as soon as possible.";
//ERROR
$ko_err_na="please enter your name";
$ko_err_em="please enter a valid email.";
$ko_err_ze="please enter at least 10 letters.";
//ERROR

$ko_gwunsch = "
Hallo,
Meine Spielwelt ist leider nicht im Farmhunter integriert.
Ich würde gerne auf: [INSERT GAMEWORLD HERE] spielen. 
Die URL zum Server lautet: [INSERT SERVERURL HERE]

Ich trage meine Email und meinen Namen oben ein damit du mich
benachrichtigen kannst sobald die Spielwelt verfügbar ist.

Vielen Dank
Mit Freundlichem Gruß aus EN
";

//KONTAKT

//NEWS
$news = "Farmhunter - News";
$ne_title="Farmhunter - Start";
$ne_wil="Welcome";

$ne_d1="you switch";
$ne_d2="in";
//xy 
$ne_d3="Seconds to the Account";
//ACCOUNT XY
$ne_willkommen = "Welcome at the Farmhunter Dashboard.
Here you can find the lastest News of the Farmhunter.
From here your way can go to the Farmlist, Farmsearch and the many other Tools.";

 $ne_error_acc1 = "Error: your Travian account was not found in the system.";
$ne_error_acc2 = "
		If your account may be deleted? If this is so, <a href=einstellungen.php?d=y&lg=1#loeschen class=link>delete your Farmhunter Account!</a><br>
		Are you sure that your account still exists? <br>
		<a href=debug.php class=link>Please inform the Admin by clicking here!</a><br>
		Thank you for your attention."; 
$ne_sendreport = "Thank you! Your report has been sent successfully. <br> It will soon be someone to worry about.";


$ne_werbemoglichkeiten="Here are some banners. You can place them in forums / signatures and websites";

//Don't translate news :)
$ne_news="
Tuesday, 12/March/2013<br />
There is now a public directory for the language files.<br />
If you are familiar with an other language and like to help making farmhunter better, feel free<br />
to translate the farmhunter. If you know GIT and Bitbucket you can simply create a push request.<br />
<a href=\"https://bitbucket.org/roest/farmhunter-lang\" target=_blank>Farmhunter language files</a><br />
For all others - just click on \"Source\" and download the code.<br />
If you can't create a push request or you didn't know how to push - simply send the file to info@farmhunter.com.<br />
There is an description how translation work on the beginning of each file.<br />
<br />
Thank you for your help!<br />

<hr>
Sunday, 18.July.2010 <br>
Today again there was a small but nice update. <br>
-The Indian worlds were include. <br>
-The notes feature is implemented. <br>
<br>
The Notes feature is available on the <a href=farmen.php>Farmlist</a> <br>
In the column \"notes\" you can now write maximum 500 characters text for your farm.<br>
To save space I display only the first 10 characters. You can see the rest of the text when <br>
you put your mouse over the text.<br>
For example you can note whether a farm is profitable or whether troops can be expected. <br>
Otherwise, the new feature is pretty self-explanatory<br>
Have fun :)<br>
<br>
<hr>
Montag, 07.Juni.2010<br>
I upload a new Update.<br>
Link: \"Open Playersearch\" works now with the old design, too<br>
Updated Farmsearch. You can now add an player on your farmlist without reload. <br>
Delete of the Farms with only 1x reload<br>
improved calculation of farmsearch<br>
Add norwegian world<br>
Update the farmlist for better overall view<br>
                            <br>
							<hr>
							Thursday, 11.März.2010<br>
							Farmhunter was translated.  It's now available in <br>
							German, English, French and Russian.<br>
							You find the possibility to switch language on the main page<br>
							
							<hr>
							Monday, 01.März.2010<br>
                            It's in planning to make the Farmhunter available in other languages.<br>
                            You're fluent in English or other languages?<br>
							message me via  <a href=kontakt.php class=link>contact formular</a> <br>
                            <hr>
							Tuesday, 23.Februar.2010<br>
                            The new design is ready and as you can see activated!<br>
                            With Internet Exlplorer 6 and 7 there could be some difficulties.<br>
							If the design isn't shown right, you can change it in  <br>
							den <a href=einstellungen.php class=link>Settings</a> to the old design <br>
                            <hr>
                            Tuesday, 16.Februar.2010<br>
                            The automatic reload by clicking on \"Attack\" in the farm list got turned off!<br>
                            The player and ally links for org got corrected.<br>
                            There is a new link for going to the town on the map.<br>
                            A direct link is unfortunately not possible because of a security measure of travian.<br>
                            Report possible errors over <a href=kontakt.php class=link>contact</a>!<br>
                            edit// error is gone, attack link correct again!<br>
                            <hr>
                            Tuesday, 9.Februar.2010<br>
                            I did a little fine tuning today!<br>
                            It's now possible to pick in the standard troops between <br>
                            Reinforcment, Normal or Raid.<br>
                            The farming list reloads automatically when you click on \"attack\" <br>
                            , with the result that the attack time reloads.<br>
                            And as you can see, I made the news show a little bit cleaner.<br>
                            <hr>
                            Sunday, 7.Februar.2010<br>
                            From now on you can connect two farm tool accounts and with that<br>
                            manage more travian worlds really easy.<br>
                            You can connect the accounts in <a href=einstellungen.php class=link>Settings</a>.
                            <br>
                            <hr>
                            Tuesday, 4.Februar.2010<br>
                            As announced the Farmhunter moved to a new domain<br>
                            It is now accessible under <a href=http://www.farmhunter.com class=link>http://www.farmhunter.com</a>.<br>
                            <br>
                            Also a new feature is included:<br>
                            You can now define standard troops for your farms.<br>
                            These troops get automatically put in.<br>
                            More information is provided under \"Standard Truppen\"<br>
                            
                            <br>
                            <br>
                                                    
                            Have fun :)";


//NEWS

//FARMEN
$farmen="Farmhunter - Farms";
$fa_um="Farms";
$fa_afe="Fields";
$fa_wi="Welcome to the farm overview.<br />  
						You got different possibilities to mark a farm .<br> 
						 You can type in a gamer ID to add all towns of a player or<br> 
						 You can write in a Town ID to add just the town.<br> ";
						 
////////////////////////////////////HIER WEITERMACHEN /////////////////////////////////////////////		

$fa_sn="User name";
$fa_addsh="Add all villages of this player";
$fa_ddh="Add this town";
$fa_si="Player ID";
$fa_di="Town ID";
$fa_dn="Town name";	
$fa_tipp_pid="you find the user id by clicking on the users profile<br> and copy the numbers at the url's end :<br> http://www.travian.de/spieler.php?uid=<b>12345</b><br>";		
$fa_tipp_did="you find the town ID by clicking on the town<br> and copying the last the number at the end of the url:<br> http://www.travian.de/karte.php?d=<b>123456</b>&c=9f<br>"; 
$fa_bil="The player <b>$_POST[playername]</b> is already in your farmlist!";
$fa_dds="the towns of the player $_POST[playername] are successfully in your farming list";
$fa_ngf="no player with the name $_POST[playername] could be found!";
$fa_bifl="This player is already in your farmlist!";
$fa_ddse="the towns of the player <b>$spielerdaten_sel[player]</b> were successfully added to your farming list";
$fa_nsf="no player with the name ID $_POST[playerid] was found!";
$fa_onumb="An ID can only consist of numbers!";	
$fa_idnf="no player with the ID $_GET[uid] was found!";
$fa_dsif="the towns of the player <b>$spielerdaten_sel_list[player]</b> were successfully added to your farming list.";
$fa_ids="the town <b>$dorfdaten[village]</b> is already in your farming list!";
$fa_ea="the town <b>$_POST[dorfname]</b> of the player <b>$dorfdaten[player]</b> was successfully added to your farming list";
$fa_dnnf="no town with the name $_POST[dorfname] could be found!";
$fa_sd="this town is already in your farming list!";
$fa_deag="the town <b>$dorfdaten_sel[village]</b> of the player <b>$dorfdaten_sel[player]</b> was successfully added to your farming list";
$fa_kdmid="no town with the ID $_POST[dorfid] could be found!";
$fa_erinfl="the town <b>$dorfdaten_del_list[village]</b> of the player <b>$dorfdaten_del_list[player]</b> was successfully added to your farming list";
$fa_kdmid2="no town with the ID $_GET[vid] could be found!";

$fa_ek="Close";
$fa_hinz="Add farm";
$fa_sor="Sort farm list by";
$fa_sso="sorting by standard";

$fa_tbl_d="Town name";
$fa_tbl_k="coordinates";
$fa_tbl_e="distance";
$fa_tbl_ei="population";
$fa_tbl_sp="player name";
$fa_tbl_no="Notes";
$fa_tbl_an="attacks";
$fa_tbl_la="last&nbsp;attack";
$fa_tbl_li="Link";
$fa_ang="Att";
$fa_heute="Today";
$fa_gestern="yesterday";
$fa_vorgestern="2 days ago";
$fa_uhr="time";

// STRUCTURE IS: "The day before yesterday - 13:45 CLOCK

$fa_nonote="No notes entered. <br> Click on <b>edit</b> to enter a new note";
$fa_note_tite="Note of the Farm";

// FARMS

// SUGGESTIONS
$vorsch = "Farmhunter - Farm proposals";
$vo_ub = "farm proposals";
$vo_eitext = "Quick instructions<br>
 <hr> <br>
- Select a village. <br>
- Choose the number of fields to be searched around your village <br>
- Choose the maximum number of residents for your farms<br>
- Press on \"Send\" <br> ";
$vo_wed = "Please select a village";
$vo_umk = "radius";
$vo_maxew = "max. Residents";
$vo_send = "Send";

$vo_d = "Village Name";
$vo_k = "Coordinates";
$vo_eigk = "Simple tile coordinates";
$vo_mf = "possible farms";
$vo_ew = "inhabitants";
$vo_sn = "Player Name";
$vo_al = "Alliance";
$vo_ent="Distance";
$vo_hin="Add";
$vo_au = "attack at";

$vo_anzf="There are $anzahl_ausgaben possible Farms in the radius of $um fields around the village";
If ($ew > 0){
$vo_anzf .=" which have less than $ew inhabitants";
}
If ($ew >0 AND $_POST[al] == 1){
$vo_anzf .=" and";
}
If ($_POST[al] == 1){
$vo_anzf .=" which are without ally";
}
$vo_anzf .=".";

$vo_allos="without ally";
$vo_spsuu="Open Playersearch";
$vo_spsu="Playersearch";
$vo_spieler="Player";
$vo_ewinfos="further infos <br>about player and ally";
$vo_spieler_suche="couldn't be found. There will be shown";
$vo_spieler_suche2="alternatives";
$vo_fasu="Open farmsearch";
$vo_nn="Name";

$vo_secureerr="This radius can't be searched for security reasons<br>please select a smaller number of fields";
$vo_tip_player="Number of villages: $sinfo_anz_dorfer<br>inhabitants: $gesamtew[gesamtew] <br>ally: $vorschlaege[alliance]<br> <table border=0><tr><td align=center>village</td><td align=center>inhabitants</td><td align=center>add</td></tr><tr><td colspan=3> <hr></td></tr> $sp_doerfer <tr><td colspan=3 align=center><a href=$link/spieler.php?uid=$vorschlaege[uid] target=_blank class=link>open in Travian</a></td></tr></table>";
$vo_tip_ally=" <table border=0><tr><td align=center>Player</td><td align=center>Open</td></tr><tr><td colspan=2> <hr></td></tr> $sp_ally_member <tr><td colspan=3 align=center><a href=$link/allianz.php?uid=$vorschlaege[aid] target=_blank class=link>Open Ally in Travian</a></td></tr></table>";
$vo_tip_allysp=" <table border=0><tr><td align=center>Player</td><td align=center>Open</td></tr><tr><td colspan=2> <hr></td></tr> $sp_ally_membersp <tr><td colspan=3 align=center><a href=$link/allianz.php?uid=$ss_doerfer[aid] target=_blank class=link>Open Ally in Travian</a></td></tr></table>";

// SUGGESTIONS

// TROOP ACTIVITY
$lastatts = "Farmtool - Troop activity";
$la_ub = "Your last 10 attacks";
// TROOP ACTIVITY

// DELETE FARMS
$loeschen = "Farmhunter - Delete Farms";
$loe_ub = "Delete farms";
$loe_wtext = "You want to remove a farm from your list, no problem! <br>
Here is a list of your registered farms <br>
Delete by clicking on the <img src=images/kreuz.gif border=0> 
(Farm gets deleted <b> After </ b> a security check). ";
$loe_erf = "The entry was sucessfully deleted";

$loe_dorf = "<b>$farmen_select_dorf[village] </b> of <a href = http://welt$alles[world].travian.de/spieler.php?uid=$farmen_select_dorf uid] target=_blank class=link> $farmen_select_dorf[player] </a> ";
$loe_adorf = "All the villages of <a href=http://welt$alles[welt].travian.de/spieler.php?uid=$farmen_select_player[uid] target=_blank class=link> $farmen_select_player[player] </a> ";
$loe_sec_dorf = "Do you really want to remove the village <b> $farmen_select_dorf[village] </b> of the player <b> $farmen_select_dorf[player] </b> from your Farmlist?";
$loe_sec_adorf = "Do you really want to remove all the villages of the player <b>$farmen_select_player[player]</b> from your Farmlist?";
$loe_ja = "Yes";
$loe_nein = "No";
// DELETE FARMS

// Force settings

$tr_title = "Farmhunter - troop settings";
$tr_ub = "troops define";
$tr_text = "Here you can define the standard troops for your farms. <br>
 With this your troops are automatically entered when you farm. <br>
 You can set a global setting, which is used <br>
 if a farm does not have own settings. <br>
 You can choose the number of troops for each farm individually. ";
 
$tr_wef = "Choose a Farm";
$tr_glob = "Globally for all farms";
$tr_global = "Global";
$tr_farmen = "farms";
$tr_ok = "OK";

$tr_2 = "support";
$tr_3 = "Normal";
$tr_4 = "raid";
$tr_do = "village";

$tr_eg = "Your Global entry was successfully saved";
$tr_up = "Your Global Entry was updated successfully";
$tr_fae = "Your entry for the Farm <b> $farm_daten [village] </ b> was successfully saved";
$tr_fau = "Your entry for the Farm <b> $farm_daten [village] </ b> was updated successfully";
// Force settings

// SETTINGS

$einstellungen = "Farmhunter - Settings";
$ei_ub = "Settings";
$ei_ne = "New Mail";
$ei_ema = "Change email";
$ei_ng = "This is not a valid e-mail";
$ei_eegb = "E-mail successfully changed. A confirmation email has been sent to $ n_email";

$ei_apw = "Old Password";
$ei_npw = "New Password";
$ei_npw2 = "New Password";
$ei_wied = "(repeat)";
$ei_pwan = "Change Password";
$ei_apwf = "The specified current password is incorrect.";
$ei_m6 = "Please enter at least 6th character as a new password";
$ei_snuei = "The specified passwords do not match.";
$ei_pwnv = "You can not use this password. Please choose a different password.";
$ei_pwerf = "Password changed successfully";

$ei_aver = "Accounts link";
$ei_hkmv = "Here you can combine two Farmtool Accounts";
$ei_nvaa = "nickname from the other account";
$ei_pwvaa = "password from the other account";
$ei_ver = "Connect";
$ei_ponnf = "Username or password incorrect";
$ei_dknsg = "You can not combine 2 identical accounts";
$ei_daerf = "The Accounts $ all [name] and $ var [name] were combined";

$ei_desa = "Design Change";
$ei_ndes = "New Design";
$ei_ades = "Old Design";

$ei_loe_pw = "Your Password";
$ei_loe_gr = "reason";
$ei_loe_send = "Delete Account";
$ei_loe_pwl = "Please enter your password";
$ei_loe_pww = "The password is incorrect.";
$ei_loe_kg = "Please give a reason so I can improve the Farmhunter further.";
$ei_loe_sure = "Are you sure?";
$ei_loe_yes = "Yes Delete";
$ei_loe_no = "No, do not delete";
$ei_loe_erf = "Your account has been successfully deleted.";
$ei_loe_accloe = "Delete Account";

// SETTINGS

// IE
$ie_title = "Farmhunter - Internet Explorer";
$ie_text = "<h3> <a href=http://www.farmhunter.com> farmhunter.com </ a> is particularly suited for Mozilla FireFox or <a href = http://www.google.de/chrome target = _blank> Google Chrome </ a> <br>
Your browser seems to be Internet Explorer. <br>
We will propose <u> free </ u> Mozilla Firefox download it by clicking on the button. </ H3>
<a href=\"#\" onclick=\"window.open('http://www.mozilla-europe.org/de/firefox/');\"> <img src =\"images / firefox. gif\"border = \" 0 \" title = \" Firefox 3.0 \" alt = \" Firefox 3.0\"> </ a> <br>

You can also go with Internet Explorer by clicking here. <br>
<a href=\"index.php\"> Carry on without FireFox </ a> <br>

It's possible that your Browser does not show our website correctly.
Should this be the case <a href=?s=d> please click here </ a>. <br> ";

// IE

// Protection of privacy assignation 
$datenschutz = "Farmhunter - Disclaimer";
$da_ub = "Disclaimer";

//Dont translate $datenschutz_text
$datenschutz_text = "
<strong> liability for content </ strong> </ p>
    <p> The contents of our pages have been created with great care.
      For the accuracy, completeness and timeliness of the content
      However, we can not guarantee. The information provided is in accordance with § 7 paragraph 1 of TMG for
      own content on this site under the general law.
      According to § § 8 to 10 TMG we as a service but not
      required transmitted or stored information to
      supervise or conduct research on the circumstances, which refer to an illegal
      Activity point. Obligations to remove or block the
      Use of information under the common law are not
      unaffected. A liability, but only from the point of
      becoming aware of a possible specific violation of law. At the point of
      becoming aware of such violations, we will immediately remove this content
      . </ p>
    <strong> liability for links </ strong> </ p>
    <p> Our site contains links to external websites over which
      We have no influence. Therefore, we're not responsible for the content of those sites.
      Only the provider or operator of those sites can be responsible for their contents.
      The linked pages were at the time of linking
      checked for possible violations of law. Illegal
      Contents were not apparent at the time of linking. Permanent
      control of the linked sites, however, without concrete evidence
       is not reasonable. Upon notification of violations
      we will remove the links immediately. </ p>
    <p> <strong> copyright </ strong> </ p>
    <p> The operator of the site content and works on these pages
      subject to German copyright law. The reproduction, modification, distribution and
      any kind of exploitation outside the limits of copyright require
      the written consent of the author or creator. Downloads
      and copies of these pages are only for private, non-commercial
      Use only. Insofar as the content on this site is created by the operator,
      be considered the copyright of third parties. In particular, third-party content as such
      marked. Should you become aware of copyright infringement, we ask for a mention.
      Upon notification of violations, we will remove such content immediately. </ P>
     </ p>
<p>
This site is private and free project for the browser game Travian.
This page does not belong to Travian Games GmbH, or in part, as a whole. This page is also not created on behalf of Travian Games GmbH.
The brand \"Travian\" is a registered trademark and is one of Travian Games GmbH All rights are with this brand of Travian Games GmbH
";
// Privacy

// EMAIL CONFIRMATION

$eb_title = "Farmtool - E-Mail confirmation";
$eb_ub = "E-mail address confirmation";
$eb_erf = "Your e-mail address has now been confirmed!";
// EMAIL CONFIRMATION

//SONSTIGES

$ubersetzt_from="Translated from English to XXX from [add your name]";
//GERNE DARFST DU AUCH EINEN BACKLINK AUF DEINEN NAMEN SETZTEN

//SONSTIGES
?>
