<?
/*
Instruktionen for translation.
Simple translate the text between ""
Replace the English Text with your language.
If you need an " in the Text please use \ bevore. (Example: The Name of the Tool is \"Farmhunter\")
Please do not remove the <br> tags.
There are some formattings and hyperlinks in the text please let them in your text.
Example: <b>Text</b> (The word "Text" is bold)
An Hyperlink looks like:
<a href=einstellungen.php class=link>Settings</a> 
Please translate one "Settings" here.
You can use some Variables in the Text. ($zufaelliges_pw)
Just let this variable as it is - just move it maybe to another position if you need it.
If you have questions: Contact me: info@protoyou.de
*/


//INDEX
$in_ub="Bem vindo ao Farmhunter, a arma de assalto do Travian";
$in_text=" Esta ferramenta dá-te a oportunidade de organizar o teu Travian
<br>
 e imediatamente iniciar os assaltos através de uma simples ligação. <br>
 Mas não é só isso! Esta ferramenta recolhe a informação directamente do Travian.<br>
 Não é necessário teclar nada.<br>
 Também há uma ferramenta de pesquisa dos alvos que te sugere as potenciais vítimas <br>
 com informação detalhada sobre jogadores e alianças<br>
 que podes adicionar à tua lista de alvos apenas com um clique.<br>
 E podes definir que tropas queres que fiquem prontas para o combate.<br>
 Esta ferramenta garante-te assaltos rápidos e fáceis<br>
 <br>
";

$in_text2="Regista-te e experimenta"; //TODO
$in_text3="100% LEGAL e GRÁTIS!"; //TODO

$in_spenden_ub = "Podes ajudar o Farmhunter com um simples €1,00!"; //TODO
 $in_spenden_text = "O Farmhunter é um projecto <u>não comercial</u>.<br> Este programa é privado e disponibilizado <u>sem custos</u>.
   Infelizmente, o projecto precisa de um servidor. É preciso tempo para manter tudo na perfeição e dinheiro para poder continuar <b>disponível para ti</b>.
  <br>Actualmente são necessários €7,00 por mês só para o servidor
	<br><br> <b>TU</b> podes ajudar o Farmhunter vivo.
   Basta UM EURO para ajudar a cobrir os custos mensais.<br /><br>
   <u>Muito obrigado pelo teu contributo.</u>"; //TODO

 $index="Farmhunter - A arma de assalto do Travian";
 $in_description="Com este Farmhunter / ferramenta de assalto podes organizar os teus assaltos rápida e facilmente.";
 $schild="Regista-te";
 
 $screenshots = "Farmhunter - Imagens"; 
  $sc_ub = "Imagens do Farmhunter.";
 $sc_text = "O funcionamento desta ferramenta será descrita melhor<br />
			se fores vendo algumas imagens.<br />
			Aqui tens algumas imagens de algumas páginas<br />
			do farmhunter. Podes ver algumas características <br />
			que terás à tua disposição no farmhunter.";
			
 $sc_totop = "no topo";
 $sc_screen1 = "Visão global - O centro da ferramenta de assalto! Podes começar todos os teus assaltos a partir daqui";
 $sc_screen2 = "Pesquisa os melhores assaltos da zona<br />
 				e regista-os na tua lista, só com um clique";
 $sc_screen3 = "No histórico dos assaltos podes ver o que<br />
 				o que tu ou o teu segundo jogador já atacaram";
 $sc_screen4 = "Claro que podes apagar um alvo a qualquer momento";
 $sc_screen5 = "Tens algumas definições na tua conta de Farmhunter";
 $sc_screen6 = "Se procuras um jogador específico para assaltar basta usares a pesquisa de jogador.";
 			
  $sc_description = "Procura as imagens do Farmhunter";
$ko_description = "Contacta o administrador do Farmhunter";
$re_description = "Regista-te no Farmhunter";
$da_description = "Protegemos os teus dados com toda a segurança";

  $in_sp1="Este projecto precisa de €7,00 por mês para pagar o servidor.";
$in_sp2= "Se quiseres apoiar este projecto <br> podes contribuir aqui. <font size=-2>Donativos já recebidos: 0/€7,00</font>";
$in_spent="clica aqui para não voltar a mostrar esta mensagem";
$in_we = "[o teu contributo aqui]";
//INDEX

//LOGIN
$lo_nick="nickname";
$lo_pass="palavra-chave";
$lo_bleiben="Continuar ligado";
$lo_passvergessen="Esqueci-me da palavra-chave";
$lo_val="ligar";

$lo_ub="Requisitar palavra-chave";
$lo_email="endereço electrónico";
$lo_verg_val="pedido";
$lo_link="Para ligar";
$lo_lo="ligar";

//LOGIN ERROR
 $lo_mss="palavra-chave or utilizador errado";
 $lo_err_el="Introduz o teu endereço electrónico sff";
$lo_err_eng="Este endereço electrónico não é válido";
$lo_err_nn="Introduz o teu nome sff";

//LOGIN ERROR

$lo_be="A tua palavra-chave para o Farmhunter.com";
$lo_em_ub="Farmhunter - palavra-chave esquecida ";
$lo_em_text="Olá $nick <br>
No <a href=http://www.farmhunter.com target=_blank>http://www.farmhunter.com</a> requisitaste uma nova palavra-chave. <br><br>
Utilizador: <b>$nick</b> <br>
palavra-chave: <b>$zufaelliges_pw</b><br><br>
<b>$strich</b><br>
<font size=-1>ATENÇÃO esta é uma mensagem automática. <br>
Não respondas a esta mensagem sff.</font><br>


<br>
Abraço,<br>
a equipa da ferramenta de assalto do Travain";
$lo_em_np="A tua palavra-chave será enviada por correio electrónico";
$lo_epn="O correio electrónico: $email electrónico não confere com o nome: $nick";
$lo_ng="nickname: $nick encontrado";

//LOGIN

//MENÜ

$me_ss="Pagina principal";
$me_so="Imagens";
$me_ko="Contacto";
$me_re="Registo";
$me_ne="Novidades";
$me_fa="Alvos";
$me_fs="Pesquisa de alvos";
$me_ta="Actividade das tropas";
$me_lo='Limpar alvos';
$me_st="Tropas";
$me_ei="Preferências";
$me_we="Servidor";
$me_lou="Sair";

//MENÜ

//REGISTRIERUNG
$reg="Farmhunter - Registo";
$re_be="<b><h3>Importante</h3></b> 
Não tens de introduzir os teus dados de acesso ao Travian. <br>
Podes escolher a palavra-chave!<br>
 Só o nome de utilizador tem de ser o mesmo<br> 
 para dares sugestões aos teus alvos <br><br>";
 
 $re_nn="nickname";
 $re_email="o teu endereço de correio electrónico";
 $re_ds="o teu servidor";
 $re_ww="escolhe um servidor";
 $re_tip_nick="Encontras o teu ID de utilizador clicando no teu perfil<br> e copiando o número que está no fim do url:<br> http://www.travian.$lang/spieler.php?uid=<b>12345</b><br><br> if you type in your user ID then your<br>Login name is the same as in Travian";
 $re_tip_pass="a tua palavra-chave está encriptada na nossa base de dados!<br>	Não está legível";
$re_pass="palavra-chave (min. 6 letras ou algarismos)";
$re_pass_wied="palavra-chave (Repete)";
$re_ouid="ou IS de utilizador do Travian";
$re_ds="o teu servidor";
$re_ha="Li o <a href=datenschutz.php target=_blank class=link>disclaimer</a> e aceitei-o.";
$re_pf="(* = OBRIGATÓRIO)";
$re_val="REGISTAR";

$re_ndabei="O teu servidor não aparece?<br><a href=\"kontakt.php?sp=1#kontakt\" class=\"link\">Escolhe o servidor</a>";


//REG ERROR
$re_err_welt="escolhe o servidor sff";
$re_err_nick="Introduz o teu nickname or ID de utilizador sff";
$re_err_nf="o teu nickname ou ID do Travian não foi encontrado no  $welt.<br>
Introduz o mesmo nickname ou ID do Travian com que te registaste no Travian, sff "; // Achtung please don’t change "$welt" ! Includes number to world"
$re_err_na="Não indicaste o servidor. O teu <br>nome não pode ser verificado";
$re_err_ge="introduz um endereço de correio electrónico válido!";
$re_err_nss="a palavra-chave não condiz";
$re_err_pl="introduz a palavra-chave, sff";
$re_err_se=" A tua palavra-chave tem de ter, no mínimo, 6 caracteres!";
$re_err_ha="aceitaste as condições.";
$re_err_nb="esse nickname já está a ser utilizado ";
$re_err_pwnv="Não podes escolher essa palavra-chave. Escolhe outra diferente, sff.";
//REG ERROR

$re_erf="Bem-vindo $user. <br>	a tua conta foi criada com sucesso."; //ACHTUNG: $user don’t change! Includes username
$re_bese="a mensagem de confirmação foi enviada para $email."; //ACHTUNG: $email don’t change! Includes E-mail

$re_em_title="Mensagem de confirmação do Farmhunter";
$re_em_text="recebeste esta mensagem, porque te registaste no \"<a href=http://farmhunter.com>http://www.farmhunter.com</a>\".<br>
<br>
Para poderes usares todas as funções, por favor activa a tua conta nesta ligação:<br><br>
<a target_blank href=http://farmhunter.com/email_b.php?3x7zl5=$emailcoded&userid=$userid>http://farmhunter.com/email_b.php?3x7zl5=$emailcoded&userid=$userid</a><br>
<br>
Cumprimentos da <br>
Equipa do Farmhunter";
//REGISTRIERUNG

//KONTAKT
$kontakt="Farmhunter - contacto / Marca";
$ko_an="Informação fornecida por § 5 TMG<br/>
Responsável:";
$ko_tel="Telefone";
$ko_haf="Disclaimer";

$ko_kont="Contacto";
$ko_oeod="por favor <b>only</b> em inglês ou alemão.";
$ko_name="o teu nome";
$ko_email="o teu endereço de correio electrónico";
$ko_ru="(importante para futuras questões!)";
$ko_na="mensagem";
$ko_wa="ATENÇÃO o teu IP será enviado por motivos de segurança ";
$ko_sen="enviado por correio electrónico";
$ko_zuruck="Voltar";
$ko_ok="Obrigado pela tua mensagem. Responderemos logo que possível.";
//ERROR
$ko_err_na="insere o teu nome sff";
$ko_err_em="introduz um endereço de correio electrónico válido sff.";
$ko_err_ze="introduz pelo menos 10 caracteres.";
//ERROR

//Don't translate this :) its for internal use only
$ko_gwunsch = "
Olá,
O Farmhunter não suporta o teu servidor.
Quero jogar no servidor: [escolha o servidor aqui]. 
O URL para o servidor é: [inserir o URL do servidor aqui]

Aceito inserir o meu nome e endereço de correio electrónico 
para poder receber um relatório se possível neste servidor.

Muito obrigado
Um grande abraço de Portugal
";

//KONTAKT

//NEWS
$news = "Farmhunter - Novidades";
$ne_title="Farmhunter - Começar";
$ne_wil="Bem-vindo";

$ne_d1="you switch";
$ne_d2="in";
//xy 
$ne_d3="Segundos para a conta";
//ACCOUNT XY
$ne_willkommen = "Bem-vindo à página principal do Farmhunter.
Aqui podes encontrar as últimas informações sobre o Farmhunter.
Daqui podes avançar para a lista de alvos, pesquisar alvos e muitas outras opções.";

 $ne_error_acc1 = "Erro: A tua conta do Travian não foi encontrada no sistema.";
$ne_error_acc2 = "
		A tua conta foi apagada? Neste caso, <a href=einstellungen.php?d=y&lg=1#loeschen class=link>apaga a tua conta no Farmhunter!</a><br>
		Tens a certeza que esta conta existe? <br>
		<a href=debug.php class=link>Clica aqui para informar o Administrador sff!</a><br>
		Obrigado pela tua atenção."; 
$ne_sendreport = "Obrigado! A tua informação foi enviada com sucesso. <br> Em breve alguém terá com que se preocupar.";


$ne_werbemoglichkeiten="Aqui tens alguns banners. Podes colocá-los em fóruns / assinaturas and páginas da internet";

//Don't translate news :)
$ne_news="
Tuesday, 12/March/2013<br />
There is now a public directory for the language files.<br />
If you are familiar with an other language and like to help making farmhunter better, feel free<br />
to translate the farmhunter. If you know GIT and Bitbucket you can simply create a push request.<br />
<a href=\"https://bitbucket.org/roest/farmhunter-lang\" target=_blank>Farmhunter language files</a><br />
For all others - just click on \"Source\" and download the code.<br />
If you can't create a push request or you didn't know how to push - simply send the file to info@farmhunter.com.<br />
There is an description how translation work on the beginning of each file.<br />
<br />
Thank you for your help!<br />

<hr>
Sunday, 18.July.2010 <br>
Today again there was a small but nice update. <br>
-The Indian worlds were include. <br>
-The notes feature is implemented. <br>
<br>
The Notes feature is available on the <a href=farmen.php>Farmlist</a> <br>
In the column \"notes\" you can now write maximum 500 characters text for your farm.<br>
To save space I display only the first 10 characters. You can see the rest of the text when <br>
you put your mouse over the text.<br>
For example you can note whether a farm is profitable or whether troops can be expected. <br>
Otherwise, the new feature is pretty self-explanatory<br>
Have fun :)<br>
<br>
<hr>
Montag, 07.Juni.2010<br>
I upload a new Update.<br>
Link: \"Open Playersearch\" works now with the old design, too<br>
Updated Farmsearch. You can now add an player on your farmlist without reload. <br>
Delete of the Farms with only 1x reload<br>
improved calculation of farmsearch<br>
Add norwegian world<br>
Update the farmlist for better overall view<br>
                            <br>
							<hr>
							Thursday, 11.März.2010<br>
							Farmhunter was translated.  It's now available in <br>
							German, English, French and Russian.<br>
							You find the possibility to switch language on the main page<br>
							
							<hr>
							Monday, 01.März.2010<br>
                            It's in planning to make the Farmhunter available in other languages.<br>
                            You're fluent in English or other languages?<br>
							message me via  <a href=kontakt.php class=link>contact formular</a> <br>
                            <hr>
							Tuesday, 23.Februar.2010<br>
                            The new design is ready and as you can see activated!<br>
                            With Internet Exlplorer 6 and 7 there could be some difficulties.<br>
							If the design isn't shown right, you can change it in  <br>
							den <a href=einstellungen.php class=link>Settings</a> to the old design <br>
                            <hr>
                            Tuesday, 16.Februar.2010<br>
                            The automatic reload by clicking on \"Attack\" in the farm list got turned off!<br>
                            The player and ally links for org got corrected.<br>
                            There is a new link for going to the town on the map.<br>
                            A direct link is unfortunately not possible because of a security measure of travian.<br>
                            Report possible errors over <a href=kontakt.php class=link>contact</a>!<br>
                            edit// error is gone, attack link correct again!<br>
                            <hr>
                            Tuesday, 9.Februar.2010<br>
                            I did a little fine tuning today!<br>
                            It's now possible to pick in the standard troops between <br>
                            Reinforcment, Normal or Raid.<br>
                            The farming list reloads automatically when you click on \"attack\" <br>
                            , with the result that the attack time reloads.<br>
                            And as you can see, I made the news show a little bit cleaner.<br>
                            <hr>
                            Sunday, 7.Februar.2010<br>
                            From now on you can connect two farm tool accounts and with that<br>
                            manage more travian worlds really easy.<br>
                            You can connect the accounts in <a href=einstellungen.php class=link>Settings</a>.
                            <br>
                            <hr>
                            Tuesday, 4.Februar.2010<br>
                            As announced the Farmhunter moved to a new domain<br>
                            It is now accessible under <a href=http://www.farmhunter.com class=link>http://www.farmhunter.com</a>.<br>
                            <br>
                            Also a new feature is included:<br>
                            You can now define standard troops for your farms.<br>
                            These troops get automatically put in.<br>
                            More information is provided under \"Standard Truppen\"<br>
                            
                            <br>
                            <br>
                                                    
                            Have fun :)";


//NEWS

//FARMEN
$farmen="Farmhunter - Alvos";
$fa_um="Alvos";
$fa_afe="Campos";
$fa_wi="Bem-vindo à visão global dos alvos.<br />  
						Tens diferentes hipóteses para marcar um alvo .<br> 
						 Podes introduzir o teu ID de jogador para adicionar todas as aldeias ou<br> 
						 podes introduzir o ID de uma aldeia para adicionar apenas essa aldeia.<br> ";
						 
////////////////////////////////////HIER WEITERMACHEN /////////////////////////////////////////////		

$fa_sn="Utilizador";
$fa_addsh="Adicionar todas as aldeias deste jogador";
$fa_ddh="Adicionar esta aldeia";
$fa_si="ID de jogador";
$fa_di="ID da aldeia";
$fa_dn="Nome da aldeia";	
$fa_tipp_pid="para encontrares o teu ID de jogador clica no perfil<br> e copia o número que está no fim do URL:<br> http://www.travian.de/spieler.php?uid=<b>12345</b><br>";		
$fa_tipp_did="para encontrares o ID das tuas aldeias clica sobre a aldeia<br> e copia o número que está no fim do URL:<br> http://www.travian.de/karte.php?d=<b>123456</b>&c=9f<br>"; 
$fa_bil="O jogador <b>$_POST[playername]</b> já está na tua lista de alvos!";
$fa_dds="a aldeia do jogador $_POST[playername] foi registada com sucesso na tua lista de alvos";
$fa_ngf="não foi encontrados nenhum jogador $_POST[playername] com esse nome!";
$fa_bifl="O jogador já está na tua lista de alvos!";
$fa_ddse="as aldeias do jogador <b>$spielerdaten_sel[player]</b> foram adicionadas com sucesso à tua lista de alvos";
$fa_nsf="não foi encontrado nenhum jogador com o ID $_POST[playerid]!";
$fa_onumb="Atenção! O ID só tem algarimos!";	
$fa_idnf="ão foi encontrado nenhum jogador com o ID $_GET[uid]!";
$fa_dsif="as aldeias do jogador <b>$spielerdaten_sel_list[player]</b> foram adicionadas com sucesso à tua lista de alvos.";
$fa_ids="a aldeia <b>$dorfdaten[village]</b> já está na tua lista de alvos!";
$fa_ea="a aldeia <b>$_POST[dorfname]</b> do jogador <b>$dorfdaten[player]</b> foi adicionada com sucesso à tua lista de alvos";
$fa_dnnf="não foi encontrada nenhuma aldeia $_POST[dorfname] com esse nome!";
$fa_sd="esta aldeia já está na tua lista de alvos!";
$fa_deag="a aldeia <b>$dorfdaten_sel[village]</b> do jogador <b>$dorfdaten_sel[player]</b> foi adicionada com sucesso à tua lista de alvos";
$fa_kdmid="não foi encontrada nenhuma aldeia $_POST[dorfid] com esse ID!";
$fa_erinfl="a aldeia <b>$dorfdaten_del_list[village]</b> do jogador <b>$dorfdaten_del_list[player]</b> foi adicionada com sucesso à tua lista de alvos";
$fa_kdmid2="não foi encontrada nenhuma aldeia $_GET[vid] com esse ID!!";

$fa_ek="Fechar";
$fa_hinz="Adicionar alvo";
$fa_sor="Organizar a lista por";
$fa_sso="Organização padrão";

$fa_tbl_d="Nome da aldeia";
$fa_tbl_k="coordenadas";
$fa_tbl_e="distância";
$fa_tbl_ei="população";
$fa_tbl_sp="nome do jogador";
$fa_tbl_no="Notas";
$fa_tbl_an="Ataques";
$fa_tbl_la="last&nbsp;ataques";
$fa_tbl_li="Ligação";
$fa_ang="Att";
$fa_heute="Hoje";
$fa_gestern="ontem";
$fa_vorgestern="anteontem";
$fa_uhr="hora";

// STRUCTURE IS: "The day before yesterday - 13:45 CLOCK

$fa_nonote="Sem notas. <br> Click on <b>edit</b> to enter a new note";
$fa_note_tite="Nota do alvo";

// FARMS

// SUGGESTIONS
$vorsch = "Farmhunter - Sugestão de alvos";
$vo_ub = "Sugestão de alvos";
$vo_eitext = "Breves instruções<br>
 <hr> <br>
- Selecciona uma aldeia. <br>
- Selecciona o número de campos a pesquisar à volta da tua aldeia <br>
- Selecciona o número máximo de habitantes dos teus alvos<br>
- Clica em \"Enviar\" <br> ";
$vo_wed = "Escolhe uma aldeia sff";
$vo_umk = "raio";
$vo_maxew = "max. residentes";
$vo_send = "Enviar";

$vo_d = "Nome da aldeia";
$vo_k = "Coordenadas";
$vo_eigk = "Simple tile coordinates";
$vo_mf = "possíveis alvos";
$vo_ew = "habitantes";
$vo_sn = "Nome do jogador";
$vo_al = "Aliança";
$vo_ent="Distância";
$vo_hin="Adicionar";
$vo_au = "ataca às";

$vo_anzf="Existem $anzahl_ausgaben possíveis alvos no raio de $um campos à volta da tua aldeia";
If ($ew > 0){
$vo_anzf .=" que têm menos de $ew habitantes";
}
If ($ew >0 AND $_POST[al] == 1){
$vo_anzf .=" e";
}
If ($_POST[al] == 1){
$vo_anzf .=" que não têm aliança";
}
$vo_anzf .=".";

$vo_allos="sem aliança";
$vo_spsuu="Abrir pesquisa de jogadores";
$vo_spsu="Pesquisa de jogadores";
$vo_spieler="Jogadores";
$vo_ewinfos="Informações futuras <br>sobre jogador e aliança";
$vo_spieler_suche="não foi encontardo. Será mostrado";
$vo_spieler_suche2="alternativas";
$vo_fasu="Abrir pesquisa de alvos";
$vo_nn="Nome";

$vo_secureerr="Por razões de segurança não é possível fazer pesquisas neste raio<br>selecciona um raio menor sff";
$vo_tip_player="Número de aldeias: $sinfo_anz_dorfer<br>habitantes: $gesamtew[gesamtew] <br>ally: $vorschlaege[alliance]<br> <table border=0><tr><td align=center>village</td><td align=center>inhabitants</td><td align=center>add</td></tr><tr><td colspan=3> <hr></td></tr> $sp_doerfer <tr><td colspan=3 align=center><a href=$link/spieler.php?uid=$vorschlaege[uid] target=_blank class=link>open in Travian</a></td></tr></table>";
$vo_tip_ally=" <table border=0><tr><td align=center>Player</td><td align=center>Open</td></tr><tr><td colspan=2> <hr></td></tr> $sp_ally_member <tr><td colspan=3 align=center><a href=$link/allianz.php?uid=$vorschlaege[aid] target=_blank class=link>Open Ally in Travian</a></td></tr></table>";
$vo_tip_allysp=" <table border=0><tr><td align=center>Player</td><td align=center>Open</td></tr><tr><td colspan=2> <hr></td></tr> $sp_ally_membersp <tr><td colspan=3 align=center><a href=$link/allianz.php?uid=$ss_doerfer[aid] target=_blank class=link>Open Ally in Travian</a></td></tr></table>";

// SUGGESTIONS

// TROOP ACTIVITY
$lastatts = "Ferramenta de alvos - Actividade das tropas";
$la_ub = "Os teus 10 últimos ataques";
// TROOP ACTIVITY

// DELETE FARMS
$loeschen = "Farmhunter - Apagar alvos";
$loe_ub = "Apagar alvos";
$loe_wtext = "Queres apagar um alvo da tua lista? Sem problema! <br>
Eis a lista de registo dos teus alvos <br>
Apaga-os clicando sobre <img src=images/kreuz.gif border=0> 
(Os alvos são apagados <b> depois </ b> da confirmação de segurança). ";
$loe_erf = "A entrada foi apagada com sucesso";

$loe_dorf = "<b>$farmen_select_dorf[village] </b> of <a href = http://welt$alles[world].travian.de/spieler.php?uid=$farmen_select_dorf uid] target=_blank class=link> $farmen_select_dorf[player] </a> ";
$loe_adorf = "Todas as aldeias de <a href=http://welt$alles[welt].travian.de/spieler.php?uid=$farmen_select_player[uid] target=_blank class=link> $farmen_select_player[player] </a> ";
$loe_sec_dorf = "Queres mesmo apagar a aldeia <b> $farmen_select_dorf[village] </b> do jogador <b> $farmen_select_dorf[player] </b> da tua lista de alvos?";
$loe_sec_adorf = "Queres mesmo apagar TODAS as aldeias do jogador <b>$farmen_select_player[player]</b> da tua lista de alvos?";
$loe_ja = "Sim";
$loe_nein = "Não";
// DELETE FARMS

// Force settings

$tr_title = "Farmhunter - definições das tropas";
$tr_ub = "definição da tropa";
$tr_text = "Aqui encontras as tropas padrão para os teus assaltos. <br>
 Com isto podes introduzir automaticamente as tuas tropas para fazerem os assaltos. <br>
 Podes escolher definições padrão, que serão usadas <br>
 se o alvo não tiver quaisquer tropas. <br>
 Podes escolher o número de tropas para cada alvo. ";
 
$tr_wef = "Escolher alvo";
$tr_glob = "Todos os alvos";
$tr_global = "Todos";
$tr_farmen = "Alvos";
$tr_ok = "OK";

$tr_2 = "Ajuda";
$tr_3 = "Normal";
$tr_4 = "Assalto";
$tr_do = "Aldeia";

$tr_eg = "Todos os teus registos foram guardados com sucesso";
$tr_up = "Todos os teus registos foram actualizados com sucesso";
$tr_fae = "A tua entrada para o alvo <b> $farm_daten [village] </ b> foi guardada com sucesso";
$tr_fau = "A tua entrada para o alvo <b> $farm_daten [village] </ b> foi actualizada com sucesso";
// Force settings

// SETTINGS

$einstellungen = "Farmhunter - Definições";
$ei_ub = "Definições";
$ei_ne = "Nova mensagem";
$ei_ema = "Mudar endereço de correio electrónico";
$ei_ng = "Este endereço de correio electrónico não é válido";
$ei_eegb = "Endereço de correio electrónico alterado com sucesso. Uma mensagem de confirmação foi enviada para $ n_email";

$ei_apw = "Palavra-chave antiga";
$ei_npw = "Palavra-chave nova";
$ei_npw2 = "Palavra-chave nova";
$ei_wied = "(repetir)";
$ei_pwan = "Mudar palavra-chave";
$ei_apwf = "A palavra-chave actual que introduziste está errada.";
$ei_m6 = "Introduzir uma palavra-chave com, pelo menos 6 caracteres";
$ei_snuei = "As palavras-chave introduzidas não condizem.";
$ei_pwnv = "Não podes escolher essa palavra-chave. Escolhe outra diferente, sff.";
$ei_pwerf = "Palavra-chave alterada com sucesso";

$ei_aver = "Ligação à conta";
$ei_hkmv = "Aqui podes combinar 2 contas da ferramenta de alvos";
$ei_nvaa = "utilizador da outra conta";
$ei_pwvaa = "palavra-chave da outra conta";
$ei_ver = "Ligar";
$ei_ponnf = "Nome de utilizador ou palavra-chave incorrecta";
$ei_dknsg = "Não podes combinar 2 contas iguais";
$ei_daerf = "As contas $ all [name] e $ var [name] estão combinadas";

$ei_desa = "Mudar imagem";
$ei_ndes = "Nova imagem";
$ei_ades = "Imagem antiga";

$ei_loe_pw = "A tua palavra-chave";
$ei_loe_gr = "Motivo";
$ei_loe_send = "Apgar conta";
$ei_loe_pwl = "Introduz a tua palavra-chave";
$ei_loe_pww = "A palavra-chave está incorrecta.";
$ei_loe_kg = "Por favor, ajuda-me a melhorar a ferramenta de alvos.";
$ei_loe_sure = "Tens a certeza?";
$ei_loe_yes = "Sim! Apagar.";
$ei_loe_no = "Não! Não apagar.";
$ei_loe_erf = "A tua conta foi apagada com sucesso.";
$ei_loe_accloe = "Apagar conta";

// SETTINGS

// Protection of privacy assignation 
$datenschutz = "Farmhunter - Declaração";
$da_ub = "Decclaração";

//Dont translate $datenschutz_text
$datenschutz_text = "
<strong> liability for content </ strong> </ p>
    <p> The contents of our pages have been created with great care.
      For the accuracy, completeness and timeliness of the content
      However, we can not guarantee. The information provided is in accordance with § 7 paragraph 1 of TMG for
      own content on this site under the general law.
      According to § § 8 to 10 TMG we as a service but not
      required transmitted or stored information to
      supervise or conduct research on the circumstances, which refer to an illegal
      Activity point. Obligations to remove or block the
      Use of information under the common law are not
      unaffected. A liability, but only from the point of
      becoming aware of a possible specific violation of law. At the point of
      becoming aware of such violations, we will immediately remove this content
      . </ p>
    <strong> liability for links </ strong> </ p>
    <p> Our site contains links to external websites over which
      We have no influence. Therefore, we're not responsible for the content of those sites.
      Only the provider or operator of those sites can be responsible for their contents.
      The linked pages were at the time of linking
      checked for possible violations of law. Illegal
      Contents were not apparent at the time of linking. Permanent
      control of the linked sites, however, without concrete evidence
       is not reasonable. Upon notification of violations
      we will remove the links immediately. </ p>
    <p> <strong> copyright </ strong> </ p>
    <p> The operator of the site content and works on these pages
      subject to German copyright law. The reproduction, modification, distribution and
      any kind of exploitation outside the limits of copyright require
      the written consent of the author or creator. Downloads
      and copies of these pages are only for private, non-commercial
      Use only. Insofar as the content on this site is created by the operator,
      be considered the copyright of third parties. In particular, third-party content as such
      marked. Should you become aware of copyright infringement, we ask for a mention.
      Upon notification of violations, we will remove such content immediately. </ P>
     </ p>
<p>
This site is private and free project for the browser game Travian.
This page does not belong to Travian Games GmbH, or in part, as a whole. This page is also not created on behalf of Travian Games GmbH.
The brand \"Travian\" is a registered trademark and is one of Travian Games GmbH All rights are with this brand of Travian Games GmbH
";
// Privacy

// EMAIL CONFIRMATION

$eb_title = "Ferramenta de alvos - endereço de correio electrónico de confirmação";
$eb_ub = "Endereço de correio electrónico de confirmação";
$eb_erf = "O teu endereço de correio electrónico está confirmado!";
// EMAIL CONFIRMATION

//SONSTIGES

$ubersetzt_from="Traduzido do inglês por [alfabeto]";
//You can leave a backlink here i will put that link on your name

//SONSTIGES
?>
